import { createTheme } from '@mui/material/styles';
import { grey } from '@mui/material/colors';
import { AppTheme } from '../models';

const getDesignTokens = (mode: AppTheme): any => ({
  palette: {
    mode,
    ...(mode === 'light'
      ? {
          // palette values for light mode
        }
      : {
          // palette values for dark mode
          primary: {
            main: '#5A8DEE',
          },
          background: {
            default: '#1A233A',
          },
          text: {
            primary: '#fff',
            secondary: grey[500],
          },
        }),
  },
  components: {
    MuiSelect: {
      styleOverrides: {
        select: {
          padding: '0.7rem',
          paddingLeft: '1rem',
        },
      },
    },
  },
});

export function getMyTheme(theme: AppTheme): any {
  return createTheme(getDesignTokens(theme));
}
