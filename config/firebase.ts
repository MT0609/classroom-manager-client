import firebase from 'firebase/compat/app';
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
} from 'firebase/storage';
import 'firebase/compat/auth';

const firebaseConfig = {
  apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
  authDomain: process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
  projectId: 'classroom-fc8d9',
  storageBucket: process.env.NEXT_PUBLIC_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: '458182773550',
  appId: '1:458182773550:web:a3f105ddaa6430dde448bf',
};

// Initialize Firebase!
const firebaseApp = firebase.initializeApp(firebaseConfig);

// Get a reference to the storage service, which is used to create references in your storage bucket!
const storage = getStorage(firebaseApp);

const uploadFile = (file: any, folder?: string) => {
  return new Promise((resolve, reject) => {
    const fileName = folder ? `${folder}/${file.name}` : `${file.name}`;
    const storageRef = ref(storage, fileName);
    const uploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on(
      'state_changed',
      (snapshot) => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      },
      (error) => {
        // Handle unsuccessful uploads
        reject('Error uploading file');
      },
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) =>
          resolve(downloadURL)
        );
      }
    );
  });
};

const downloadFile = (fileName: string, folderName?: string) => {
  const name = folderName ? `${folderName}/${fileName}` : fileName;
  const starsRef = ref(storage, name);

  getDownloadURL(starsRef)
    .then((url) => {
      const xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = (event) => {
        let blob = new Blob([xhr.response]);
        let href = URL.createObjectURL(blob);
        let a = document.createElement('a') as HTMLAnchorElement;
        a.href = href;
        a.setAttribute('download', fileName);
        a.click();
        URL.revokeObjectURL(href);
      };
      xhr.open('GET', url);
      xhr.responseType = 'blob';
      xhr.send();
    })
    .catch((error) => {
      console.log('download error code', error.code);
      switch (error.code) {
        case 'storage/object-not-found':
          // File doesn't exist
          break;
        case 'storage/unauthorized':
          // User doesn't have permission to access the object
          break;
        case 'storage/canceled':
          // User canceled the upload
          break;

        // ...

        case 'storage/unknown':
          // Unknown error occurred, inspect the server response
          break;
      }
    });
};

export { firebase, uploadFile, downloadFile };
