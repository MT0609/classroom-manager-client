import { useEffect, useState } from 'react';
import Cookies from 'js-cookie';
import jwt from 'jsonwebtoken';

function OnAutoRefresh() {
  const [accessToken] = useState(
    Cookies.get(process.env.NEXT_PUBLIC_ACCESS_TOKEN || '')
  );
  const [refreshToken] = useState(
    Cookies.get(process.env.NEXT_PUBLIC_REFRESH_TOKEN || '')
  );

  useEffect(() => {
    try {
      if (!accessToken || !refreshToken) return;

      jwt.verify(accessToken, process.env.NEXT_PUBLIC_JWT_KEY || '');
      jwt.verify(refreshToken, process.env.NEXT_PUBLIC_JWT_KEY || '');

      window.location.reload();
    } catch (error) {
      console.log(error);
      window.localStorage.clear();
    }
  }, [accessToken, refreshToken]);
}

export default OnAutoRefresh;
