import { useEffect } from 'react';
import { firebase } from '../config/firebase';
import { useAppDispatch } from '../redux/store';
import accountApi from '../api/accountApi';
import { storeAuthInfo } from '../redux/slices/auth';
import { showErrorNoti } from '../redux/slices/notification';

function OnAuthSocial() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    const unregisterAuthObserver = firebase
      .auth()
      .onAuthStateChanged(async (user) => {
        try {
          const token = await user?.getIdToken();
          if (!token) return;

          const body = {
            externalToken: token,
          };
          const result: any = await accountApi.socialSignIn(body);

          if (result) {
            dispatch(storeAuthInfo(result.user));
            return;
          }

          dispatch(showErrorNoti('Login fail! Try again!'));
        } catch (error: any) {
          console.log(error.message);
          dispatch(
            showErrorNoti(
              JSON.parse(error.message).message || 'Login fail! Try again!'
            )
          );
          firebase.auth().signOut();
          window.localStorage.clear();
        }
      });

    return () => unregisterAuthObserver(); // Make sure we un-register Firebase observers when the component unmounts.
  }, []);
}

export default OnAuthSocial;
