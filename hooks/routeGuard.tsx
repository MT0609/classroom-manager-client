import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import HomePage from '../pages';
import LoginPage from '../pages/auth/login';
import RegisterPage from '../pages/auth/register';
import { useAppSelector, useAppDispatch } from '../redux/store';
import { getMyInfo } from '../redux/slices/auth';
import LoadingPage from '../components/loading/loadingPage';
import ActivateAccountPage from '../pages/auth/activate';
import ResendActivateAccountPage from '../pages/auth/resend-activate';
import ResetPasswordPage from '../pages/auth/reset-password';
import SendResetPasswordPage from '../pages/auth/send-reset-mail';

const RouteGuard: React.FC = ({ children }) => {
  const dispatch = useAppDispatch();
  const { isAuthenticated, isFetching } = useAppSelector(
    (state) => state.authInfo
  );

  const router = useRouter();
  const { pathname } = router;

  useEffect(() => {
    dispatch(getMyInfo());
  }, [dispatch]);

  useEffect(() => {
    if (!isAuthenticated && pathname !== '/auth/login') {
      if (isFetching) return;
      if (
        pathname === '/auth/register' ||
        pathname === '/auth/activate' ||
        pathname === '/auth/resend-activate' ||
        pathname === '/auth/reset-password' ||
        pathname === '/auth/send-reset-mail'
      )
        return;
      router.push('/auth/login');
    }

    if (
      isAuthenticated &&
      // (pathname === '/auth/login' || pathname === '/auth/register')
      pathname.startsWith('/auth')
    )
      router.replace('/');
  }, [pathname, router, isAuthenticated, isFetching]);

  if (isFetching) return <LoadingPage />;

  if (!isAuthenticated) {
    if (pathname === '/auth/register') return <RegisterPage />;
    if (pathname === '/auth/activate') return <ActivateAccountPage />;
    if (pathname === '/auth/resend-activate')
      return <ResendActivateAccountPage />;
    if (pathname === '/auth/reset-password') return <ResetPasswordPage />;
    if (pathname === '/auth/send-reset-mail') return <SendResetPasswordPage />;
    if (pathname !== '/auth/login') return <LoadingPage />;
    return <LoginPage />;
  }

  if (
    isAuthenticated &&
    (pathname === '/auth/login' || pathname === '/auth/register')
  )
    return <HomePage />;

  return <>{children}</>;
};

export default RouteGuard;
