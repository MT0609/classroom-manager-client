import React from 'react';
import { Button, ButtonProps, CircularProgress } from '@mui/material';

interface Props extends ButtonProps {
  loading: boolean;
  title: string;
}

const LoadingButton: React.FC<Props> = (props) => {
  const { loading = false, title, ...otherProps } = props;

  return (
    <Button {...otherProps} disabled={loading}>
      {loading && <CircularProgress size={20} style={{ marginRight: 10 }} />}
      {title}
    </Button>
  );
};

export default LoadingButton;
