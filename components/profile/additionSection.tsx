import React, { useState } from 'react';
import { Button, NativeSelect } from '@mui/material';
import { ViewType } from './interface';
import styles from './index.module.scss';

interface Props {
  userInfo: any;
  mode: ViewType;
  onChangeMode: () => void;
  onSave: (obj: any) => void;
}

const AdditionProfileSection: React.FC<Props> = (props) => {
  const { userInfo, mode, onChangeMode, onSave } = props;

  const [themeSelect, setThemeSelect] = useState<string>();

  const changeTheme = (e: any) => {
    setThemeSelect(e.target.value);
  };

  const switchViewMode = () => {
    setThemeSelect(userInfo.theme);
    onChangeMode();
  };

  const saveSettings = () => {
    onSave({ theme: themeSelect });
    onChangeMode();
  };

  return (
    <div className={`${styles.section}`}>
      <div className={styles.main}>
        <div className={styles.field}>
          <span className={styles['field__name']}>Theme</span>
          {mode === 'view' ? (
            <>
              <NativeSelect
                disabled
                style={{ minWidth: '10rem' }}
                value={userInfo.theme}
              >
                <option value={'light'}>Light</option>
                <option value={'dark'}>Dark</option>
              </NativeSelect>
            </>
          ) : (
            <>
              <NativeSelect
                style={{ minWidth: '10rem' }}
                onChange={changeTheme}
              >
                <option value={'light'}>Light</option>
                <option value={'dark'}>Dark</option>
              </NativeSelect>
            </>
          )}
        </div>
      </div>

      <div className={styles.buttons}>
        {mode === 'view' ? (
          <Button onClick={switchViewMode}>Edit</Button>
        ) : (
          <>
            <Button
              variant="contained"
              style={{ marginRight: '1rem' }}
              onClick={saveSettings}
            >
              Save
            </Button>
            <Button onClick={switchViewMode}>Cancel</Button>
          </>
        )}
      </div>
    </div>
  );
};

export default AdditionProfileSection;
