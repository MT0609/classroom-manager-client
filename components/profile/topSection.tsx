import React, { useState } from 'react';
import Image from 'next/image';
import { Button, TextField, IconButton, Grid } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import userLogo from '/public/user.png';
import { uploadFile } from '../../config/firebase';
import { ViewType } from './interface';
import styles from './index.module.scss';

interface Props {
  userInfo: any;
  mode: ViewType;
  onChangeMode: () => void;
  onSave: (body: any) => void;
}

const ProfileTopSection: React.FC<Props> = (props) => {
  const { userInfo, mode, onChangeMode, onSave } = props;

  const [firstNameInput, setFirstNameInput] = useState<string>();
  const [lastNameInput, setLastNameInput] = useState<string>();
  const [studentIdInput, setStudentIdInput] = useState<string>();
  const [loading, setLoading] = useState<boolean>(false);

  const switchViewMode = () => {
    setFilePreview(undefined);
    setFile('');
    onChangeMode();
  };

  const [filePreview, setFilePreview] = useState<any>();
  const [file, setFile] = useState<any>('');

  const handleImageAsFile = (e: any) => {
    const image: any = e.target?.files[0];
    if (!image) return;
    setFile(image);
    setFilePreview(URL.createObjectURL(image));
  };

  const saveChanges = async () => {
    try {
      setLoading(true);
      let fileLink;
      if (file) fileLink = await uploadFile(file, 'images');
      onSave({
        firstName: firstNameInput || userInfo.firstName,
        lastName: lastNameInput || userInfo.lastName,
        studentId: studentIdInput || userInfo.studentId || '',
        avatarUrl: fileLink || userInfo.avatar || '',
      });
      onChangeMode();
      setLoading(false);
    } catch (error: any) {
      console.log(error.message);
      alert('some error while uploading file...');
    }
  };

  return (
    <div className={`${styles.section} ${styles['section--center']}`}>
      <div className={styles.main}>
        {mode === 'view' ? (
          <>
            <div className={styles.image}>
              <Image
                className="profile-image"
                src={userInfo.avatar || userLogo}
                data-src={userInfo.avatar || userLogo}
                data-srcset={userInfo.avatar || userLogo}
                alt="avatar"
                width={100}
                height={100}
              />
            </div>
            <p>
              {userInfo.firstName} {userInfo.lastName}
            </p>
            <p>Email: {userInfo.email}</p>
            {userInfo.studentId ? (
              <p>Student ID: {userInfo.studentId}</p>
            ) : (
              <></>
            )}
          </>
        ) : (
          <>
            <div className={styles.image}>
              <input
                id="profile-avatar"
                style={{ display: 'none' }}
                type="file"
                onChange={handleImageAsFile}
                accept="image/png,image/jpeg,image/jpg,image/bmp"
              />
              <div className={styles.image__upload}>
                <IconButton
                  aria-label="delete"
                  onClick={() =>
                    document.getElementById('profile-avatar')?.click()
                  }
                >
                  <EditIcon />
                </IconButton>
              </div>
              <Image
                className="profile-image"
                src={filePreview || userInfo.avatar || userLogo}
                data-src={filePreview || userInfo.avatar || userLogo}
                data-srcset={filePreview || userInfo.avatar || userLogo}
                alt="avatar"
                width={100}
                height={100}
              />
            </div>

            <Grid container spacing={2}>
              <Grid item xs={6}>
                <TextField
                  label="First name"
                  fullWidth
                  defaultValue={userInfo.firstName}
                  onChange={(e) => setFirstNameInput(e.target.value)}
                  variant="standard"
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  label="Last name"
                  fullWidth
                  defaultValue={userInfo.lastName}
                  onChange={(e) => setLastNameInput(e.target.value)}
                  variant="standard"
                />
              </Grid>
            </Grid>
            <TextField
              label="Student Id"
              defaultValue={userInfo.studentId}
              onChange={(e) => setStudentIdInput(e.target.value)}
              variant="standard"
            />
          </>
        )}
      </div>

      <div className={styles.buttons}>
        {mode === 'view' ? (
          <Button onClick={switchViewMode}>Edit</Button>
        ) : (
          <>
            <Button
              variant="contained"
              onClick={saveChanges}
              style={{ marginRight: '1rem' }}
              disabled={loading}
            >
              Save
            </Button>
            <Button onClick={switchViewMode} disabled={loading}>
              Cancel
            </Button>
          </>
        )}
      </div>
    </div>
  );
};

export default ProfileTopSection;
