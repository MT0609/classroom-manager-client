import React, { useState } from 'react';
import { Button, FormControl, TextField } from '@mui/material';
import { ViewType } from './interface';
import { IChangePassword } from '../../models/form';
import styles from './index.module.scss';

interface Props {
  mode: ViewType;
  onChangeMode: () => void;
  onSave: (obj: IChangePassword) => void;
}

const PasswordSection: React.FC<Props> = (props) => {
  const { mode, onChangeMode, onSave } = props;

  const [oldPassword, setOldPassword] = useState<string>('');
  const [newPassword, setNewPassword] = useState<string>('');
  const [confirmNewPassword, setConfirmNewPassword] = useState<string>('');
  const [error, setError] = useState<string>('');

  const clearInputs = () => {
    setError('');
    setOldPassword('');
    setNewPassword('');
    setConfirmNewPassword('');
  };

  const switchViewMode = () => {
    setError('');
    clearInputs();
    onChangeMode();
  };

  const saveSettings = () => {
    if (newPassword !== confirmNewPassword) {
      setError('New password and confirmed password not match');

      return;
    }
    if (newPassword.length < 6) {
      setError('New password must be at least 6 characters');

      return;
    }
    clearInputs();
    onChangeMode();
    onSave({
      oldPassword,
      newPassword,
      confirmNewPassword,
    });
  };

  return (
    <div className={`${styles.section}`}>
      <div className={styles.main}>
        <span className={styles['field__name']}>Password</span>
        <FormControl fullWidth>
          <TextField
            style={{ marginBottom: '10px' }}
            size="small"
            type="password"
            label="Current password"
            disabled={mode === 'view'}
            value={oldPassword}
            onChange={(e) => setOldPassword(e.target.value)}
          />
        </FormControl>
        <FormControl fullWidth>
          <TextField
            style={{ marginBottom: '10px' }}
            size="small"
            type="password"
            label="New password"
            disabled={mode === 'view'}
            value={newPassword}
            onChange={(e) => setNewPassword(e.target.value)}
          />
        </FormControl>
        <FormControl fullWidth>
          <TextField
            style={{ marginBottom: '5px' }}
            size="small"
            type="password"
            label="Confirm new password"
            disabled={mode === 'view'}
            value={confirmNewPassword}
            onChange={(e) => setConfirmNewPassword(e.target.value)}
          />
        </FormControl>

        {error ? (
          <div>
            <span className="error">{error}</span>
          </div>
        ) : (
          <></>
        )}
      </div>

      <div className={styles.buttons}>
        {mode === 'view' ? (
          <Button onClick={switchViewMode}>Edit</Button>
        ) : (
          <>
            <Button
              variant="contained"
              style={{ marginRight: '1rem' }}
              onClick={saveSettings}
            >
              Save
            </Button>
            <Button onClick={switchViewMode}>Cancel</Button>
          </>
        )}
      </div>
    </div>
  );
};

export default PasswordSection;
