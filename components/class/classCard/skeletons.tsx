import * as React from 'react';
import { Grid, Box, Skeleton } from '@mui/material';

function ClassCardLoading() {
  return (
    <Grid container>
      {Array.from(new Array(3)).map((_, index) => (
        <Box key={index} sx={{ width: 210, marginRight: 2 }}>
          <Skeleton variant="rectangular" width={210} height={118} />
          <Box sx={{ pt: 0.5 }}>
            <Skeleton />
            <Skeleton width="60%" />
          </Box>
        </Box>
      ))}
    </Grid>
  );
}

export default ClassCardLoading;
