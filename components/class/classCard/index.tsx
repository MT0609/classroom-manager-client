import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { Box, Tooltip } from '@mui/material';
import { IClassroom } from '../../../models';
import UserAvt from '/public/user.png';
import ShieldImage from '/public/shield.png';
import { checkIsCreator } from '../../../utils';
import styles from './index.module.scss';

const ClassCard: React.FC<{ classroom: IClassroom }> = (props) => {
  const { classroom } = props;
  const router = useRouter();

  const [isCreator, setIsCreator] = useState<boolean>();

  useEffect(() => {
    if (classroom.id) setIsCreator(checkIsCreator(classroom.createdUser?.id));
  }, [classroom]);

  return (
    <div className={styles.card}>
      <div className={styles.top}>
        <div
          className={styles.top__main}
          onClick={() => {
            router.push({
              pathname: '/class/[id]/home',
              query: { id: classroom.id },
            });
          }}
        >
          <div className={styles.title}>{classroom.name}</div>
          <div className={styles.subject}>{classroom.subject}</div>
        </div>
        <div>
          {classroom.createdUser?.firstName} {classroom.createdUser?.lastName}
        </div>
        <div
          className={`${styles.icons} ${isCreator && styles.icons__withShield}`}
        >
          {isCreator ? (
            <Tooltip title="You created this class">
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Image alt="teacher" src={ShieldImage} width={30} height={30} />
              </Box>
            </Tooltip>
          ) : (
            <></>
          )}
          <Image
            className="profile-image"
            alt="teacher_avt"
            src={classroom.createdUser?.avatarUrl || UserAvt}
            width={40}
            height={40}
          />
        </div>
      </div>
      <div className={styles.bottom}></div>
    </div>
  );
};

export default ClassCard;
