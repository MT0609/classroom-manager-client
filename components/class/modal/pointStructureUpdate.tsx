import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Checkbox,
} from '@mui/material';
import { IPointStructureForm } from '../../../models/form';
import { IPointStructure } from '../../../models';

interface Props {
  open: boolean;
  type: string;
  pointStructureInfo: IPointStructure;
  handleClose: () => void;
  clickCreate: (data: IPointStructure) => void;
  clickEdit: (data: any) => void;
}

const PointStructureModal: React.FC<Props> = (props) => {
  const {
    open = false,
    type,
    pointStructureInfo,
    handleClose,
    clickCreate,
    clickEdit,
  } = props;

  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  } = useForm<IPointStructureForm>();

  useEffect(() => {
    if (type !== 'create') {
      setValue('name', pointStructureInfo.name || '');
      setValue('point', pointStructureInfo.point || 0);
    }
  }, [pointStructureInfo, type]);

  const onSubmit = (data: any) => {
    reset();
    if (type === 'create')
      clickCreate({
        ...data,
        code: '',
      });
    else
      clickEdit({
        code: pointStructureInfo.code,
        name: data.name,
        point: data.point,
        isFinalized: data.isFinalized,
      });
    onClose();
  };

  const onClose = () => {
    reset();
    handleClose();
  };

  return (
    <Dialog open={open} fullWidth={true} maxWidth="sm" onClose={onClose}>
      <DialogTitle style={{ paddingBottom: 0 }}>
        {type === 'create'
          ? 'Create new point structure'
          : 'Edit point structure'}
      </DialogTitle>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogContent>
          <TextField
            style={{ marginTop: '10px' }}
            label="Point Structure name"
            fullWidth
            autoFocus
            {...register('name', {
              required: 'Point Structure name is required',
            })}
            error={Boolean(errors.name)}
            helperText={errors.name?.message}
          />
          <TextField
            style={{ marginTop: '20px' }}
            label="Point"
            type="number"
            fullWidth
            {...register('point', {
              required: 'Point is required',
            })}
            error={Boolean(errors.point)}
            helperText={errors.point?.message}
          />
          <FormControlLabel
            control={
              <Checkbox
                {...register('isFinalized')}
                defaultChecked={pointStructureInfo.isFinalized}
              />
            }
            label="Finalized"
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button type="submit" variant="contained">
            {type === 'create' ? 'Create' : 'Edit'}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default PointStructureModal;
