import * as React from 'react';
import { useForm } from 'react-hook-form';
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';
import { useAppSelector } from '../../../redux/store';
// import styles from './index.module.scss';

interface Props {
  data: any;
  open: boolean;
  error: string;
  onRequestGradeReview: (data: any) => void;
  handleClose: () => void;
}

const GradeReviewPopup: React.FC<Props> = (props) => {
  const authUser = useAppSelector((state) => state.authInfo);
  const { data, open = false, onRequestGradeReview, handleClose } = props;

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<{ expectedGrade: number; explanation: string }>();

  const onSubmit = async (submitData: {
    expectedGrade: number;
    explanation: string;
  }) => {
    onRequestGradeReview({
      studentId: authUser.studentId,
      code: data.code,
      expectedGrade: submitData.expectedGrade,
      explanation: submitData.explanation,
    });
    reset();
  };

  const onClose = () => {
    reset();
    handleClose();
  };

  return (
    <Dialog open={open} fullWidth={true} maxWidth="sm" onClose={onClose}>
      <DialogTitle style={{ paddingBottom: 0 }}>
        Request Grade Review
      </DialogTitle>

      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogContent>
          <TextField
            label="Expected grade"
            type="number"
            fullWidth
            {...register('expectedGrade', {
              required: 'Expected Grade is required',
            })}
            error={Boolean(errors.expectedGrade)}
            helperText={errors.expectedGrade?.message}
          />

          <TextField
            style={{ marginTop: '20px' }}
            label="Explanation"
            fullWidth
            {...register('explanation', {
              required: 'Explanation is required',
            })}
            error={Boolean(errors.explanation)}
            helperText={errors.explanation?.message}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={onClose}>Close</Button>
          <Button type="submit" variant="contained">
            Submit
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default GradeReviewPopup;
