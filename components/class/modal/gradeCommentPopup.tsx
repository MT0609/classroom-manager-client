import * as React from 'react';
import { useForm } from 'react-hook-form';
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogTitle,
  Divider,
} from '@mui/material';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import LoadingButton from '../../button/loadingButton';
import styles from './index.module.scss';

interface Props {
  data: any;
  open: boolean;
  isClassCreator: boolean;
  onCommentGradeReview: (data: any) => void;
  onApproveGradeReview: (data: any) => void;
  handleClose: () => void;
}

const GradeCommentPopup: React.FC<Props> = (props) => {
  const {
    data,
    open = false,
    isClassCreator,
    onCommentGradeReview,
    onApproveGradeReview,
    handleClose,
  } = props;

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<{ comment: string }>();

  const onSubmit = async (formData: any, markCode: string) => {
    const submitData = {
      studentId: data.student.user?.studentId,
      code: markCode,
      comment: formData.comment,
    };
    onCommentGradeReview(submitData);
    reset();
  };

  const [value, setValue] = React.useState('1');

  const handleChange = (event: any, newValue: any) => {
    setValue(newValue);
  };

  const onClose = () => {
    reset();
    handleClose();
  };

  const approveGradeReview = (markCode: string, expectedPoint: number) => {
    const submitData = {
      studentId: data.student.user?.studentId,
      code: markCode,
      point: expectedPoint,
      markApproval: true,
    };
    onApproveGradeReview(submitData);
  };

  if (!data?.pointStructure)
    return (
      <Dialog open={open} fullWidth={true} maxWidth="sm" onClose={onClose}>
        <DialogTitle style={{ paddingBottom: 0 }}>Grade Comments</DialogTitle>
      </Dialog>
    );

  return (
    <Dialog open={open} fullWidth={true} maxWidth="sm" onClose={onClose}>
      <DialogTitle style={{ paddingBottom: 0 }}>Grade Review</DialogTitle>

      <Box sx={{ width: '100%' }}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList onChange={handleChange} aria-label="lab API tabs example">
              {data.pointStructure.map((item: any, index: number) => (
                <Tab
                  key={`tab-${index + 1}`}
                  label={item.name}
                  value={`${index + 1}`}
                />
              ))}
            </TabList>
          </Box>
          {data?.student?.grade?.map((item: any, index: number) => {
            const comments = item.comments;

            return (
              <TabPanel key={`panel-${index + 1}`} value={`${index + 1}`}>
                <>
                  {item.expectedGrade ? (
                    <div className={styles.grade__top}>
                      <div>
                        <p>Expected grade: {item.expectedGrade}</p>
                        <p>Explanation: {item.explanation}</p>
                      </div>

                      {isClassCreator ? (
                        <div className={styles.grade__approve}>
                          <LoadingButton
                            onClick={() =>
                              approveGradeReview(item.code, item.expectedGrade)
                            }
                            variant="contained"
                            title="approve"
                            loading={false}
                          />
                        </div>
                      ) : null}
                    </div>
                  ) : null}

                  <Divider sx={{ borderBottomWidth: 2, marginTop: '10px' }} />

                  <div className={styles.comments}>
                    {comments ? (
                      comments.length ? (
                        comments.map((comment: any, i: number) => (
                          <>
                            <div
                              key={`panel-${index + 1}-comment-${i}`}
                              className={styles.comments__item}
                            >
                              <p>
                                <span className={styles.comments__time}>
                                  {new Date(comment.time).toLocaleString()}
                                </span>
                                <span> - </span>
                                <span>{comment.senderName}</span>
                              </p>
                              <p>{comment.content}</p>
                            </div>

                            {i < comments.length - 1 ? (
                              <Divider sx={{ borderBottomWidth: 2 }} />
                            ) : null}
                          </>
                        ))
                      ) : (
                        <div className={styles.comments__item}>
                          <p>No new comments on this grade composition</p>
                        </div>
                      )
                    ) : (
                      <div className={styles.comments__item}>
                        <p>No new comments on this grade composition</p>
                      </div>
                    )}
                  </div>

                  <Divider sx={{ borderBottomWidth: 2 }} />

                  <form
                    onSubmit={handleSubmit((data: any) =>
                      onSubmit(data, item.code)
                    )}
                    className={styles.form__comment}
                  >
                    <TextField
                      {...register('comment', {
                        required: 'comment is required',
                      })}
                      label="New comment"
                      variant="standard"
                      error={Boolean(errors.comment)}
                      helperText={errors.comment?.message}
                    />
                    <LoadingButton
                      variant="outlined"
                      type="submit"
                      title="Send"
                      loading={false}
                    />
                  </form>
                </>
              </TabPanel>
            );
          })}
        </TabContext>
      </Box>

      <DialogActions>
        <Button onClick={onClose}>Close</Button>
      </DialogActions>
    </Dialog>
  );
};

export default GradeCommentPopup;
