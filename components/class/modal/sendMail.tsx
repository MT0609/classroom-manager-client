import { useRef, useState } from 'react';
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Box,
} from '@mui/material';
import { validateEmail } from '../../../utils';
import { ISendInvitationMail } from '../../../models/form';
import { UserRole } from '../../../models';

interface Props {
  open: boolean;
  role: UserRole;
  handleClose: () => void;
  classCode: string;
  onSendMail: (data: ISendInvitationMail) => void;
}

const SendMailModal: React.FC<Props> = (props) => {
  const { open = false, role, classCode, handleClose, onSendMail } = props;

  const emailInputRef = useRef<any>();
  const [emailInput, setEmailInput] = useState<string>('');
  const [inputError, setInputError] = useState<string>('');

  const onClose = () => {
    handleClose();
  };

  const sendMail = () => {
    if (!validateEmail(emailInput)) {
      setInputError('Email format not correct');
      return;
    }
    setEmailInput('');
    onClose();
    onSendMail({ email: emailInput, code: classCode, role });
  };

  return (
    <Dialog open={open} fullWidth={true} maxWidth="sm" onClose={onClose}>
      <DialogTitle>Invite one to your classroom with role {role}</DialogTitle>
      <DialogContent>
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <TextField
            ref={emailInputRef}
            autoFocus
            style={{ marginTop: '5px', marginRight: '10px' }}
            value={emailInput}
            label="Recipient's Email"
            fullWidth
            error={Boolean(inputError)}
            helperText={inputError}
            onChange={(event) => {
              setEmailInput(event.target.value);
              setInputError('');
            }}
          />
        </Box>
      </DialogContent>

      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button
          type="submit"
          variant="contained"
          disabled={emailInput.trim() === ''}
          onClick={sendMail}
        >
          Send
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SendMailModal;
