import * as React from 'react';
import { useForm } from 'react-hook-form';
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@mui/material';
import { IClassCreate } from '../../../models/form';

interface Props {
  open: boolean;
  handleClose: () => void;
  clickCreate: (data: IClassCreate) => void;
}

const CreateClassModal: React.FC<Props> = (props) => {
  const { open = false, handleClose, clickCreate } = props;

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<IClassCreate>();

  const onSubmit = (data: IClassCreate) => {
    reset();
    clickCreate(data);
  };

  const onClose = () => {
    reset();
    handleClose();
  };

  return (
    <Dialog open={open} fullWidth={true} maxWidth="sm" onClose={onClose}>
      <DialogTitle style={{ paddingBottom: 0 }}>Create a class</DialogTitle>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogContent>
          <DialogContentText>Enter classroom information</DialogContentText>
          <TextField
            style={{ marginTop: '10px' }}
            label="Name *"
            type="text"
            placeholder="Class name"
            fullWidth
            inputProps={{ minLength: 6 }}
            {...register('name', { required: 'Class name is required' })}
            error={Boolean(errors.name)}
            helperText={errors.name?.message}
          />
          <TextField
            style={{ marginTop: '10px' }}
            label="Description"
            fullWidth
            placeholder="Class description"
            {...register('description')}
          />
          <TextField
            style={{ marginTop: '10px' }}
            label="Subject"
            fullWidth
            {...register('subject')}
          />
          <TextField
            style={{ marginTop: '10px' }}
            label="Room"
            fullWidth
            {...register('room')}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button type="submit" variant="contained">
            Create
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default CreateClassModal;
