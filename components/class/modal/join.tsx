import * as React from 'react';
import { useForm } from 'react-hook-form';
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@mui/material';
import { IClassJoin } from '../../../models/form';
import styles from './index.module.scss';

interface Props {
  open: boolean;
  error: string;
  handleClose: () => void;
  clickJoin: (data: IClassJoin) => void;
  clearError: () => void;
}

const JoinClassModal: React.FC<Props> = (props) => {
  const {
    open = false,
    error = '',
    handleClose,
    clickJoin,
    clearError,
  } = props;

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<IClassJoin>();

  const onSubmit = (data: IClassJoin) => {
    reset();
    clickJoin(data);
  };

  const onClose = () => {
    reset();
    handleClose();
  };

  return (
    <Dialog open={open} fullWidth={true} maxWidth="sm" onClose={onClose}>
      <DialogTitle style={{ paddingBottom: 0 }}>Join a class</DialogTitle>
      <form onSubmit={handleSubmit(onSubmit)} onChange={() => clearError()}>
        <DialogContent>
          <DialogContentText>Enter classroom code</DialogContentText>
          <TextField
            style={{ marginTop: '10px' }}
            label="Code"
            fullWidth
            {...register('code', { required: 'Class code is required' })}
            error={Boolean(errors.code)}
            helperText={errors.code?.message}
          />

          {error ? <span className="error">{error}</span> : ''}
        </DialogContent>

        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button type="submit" variant="contained">
            Join
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default JoinClassModal;
