import React from 'react';
import Image from 'next/image';
import UserAvt from '/public/user.png';
import { IconButton } from '@mui/material';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import { IUserInfo, UserRole } from '../../../models';
import styles from './index.module.scss';

interface Props {
  title: string;
  people: IUserInfo[];
  role: UserRole;
  isClassCreator: boolean;
  onOpenSendMailDialog: (role: UserRole) => void;
}

const MemberList: React.FC<Props> = (props) => {
  const { title, people, role, isClassCreator, onOpenSendMailDialog } = props;

  const openSendMailDialog = () => {
    onOpenSendMailDialog(role);
  };

  return (
    <div>
      <div className={styles.top}>
        <div>{title}</div>
        {isClassCreator ? (
          <IconButton onClick={openSendMailDialog}>
            <GroupAddIcon />
          </IconButton>
        ) : (
          <></>
        )}
      </div>
      <div className={styles.content}>
        {people.length > 0 ? (
          people.map((person, index) => (
            <div key={`${title}-${index}`} className={styles.person}>
              <Image
                className="profile-image"
                src={person.avatarUrl || UserAvt}
                alt="user"
                width={40}
                height={40}
              />
              <span>
                {person.firstName} {person.lastName}
              </span>
            </div>
          ))
        ) : (
          <div>No students have joined class</div>
        )}
      </div>
    </div>
  );
};

export default MemberList;
