import React from 'react';
import { IPointStructure } from '../../../models';
import PointStructureBox from './pointStructureBox';
import styles from './index.module.scss';

interface Props {
  pointStructures: IPointStructure[];
  onDragEnd: (dragIndex: number, hoverIndex: number) => void;
  onOpenEditDialog: (index: number | string) => void;
  onDeletePointStructure: (index: number | string) => void;
}

const PointStructureList: React.FC<Props> = (props) => {
  const {
    pointStructures,
    onDragEnd,
    onOpenEditDialog,
    onDeletePointStructure,
  } = props;

  const onMove = (dragIndex: number, hoverIndex: number) => {
    onDragEnd(dragIndex, hoverIndex);
  };

  const onOpenEdit = (index: number | string) => {
    onOpenEditDialog(index);
  };

  const onDelete = (index: number | string) => {
    onDeletePointStructure(index);
  };

  return (
    <div className={styles.cards}>
      {pointStructures.map((pointStructure: any, index: number) => (
        <PointStructureBox
          pointStructure={pointStructure}
          key={pointStructure.code}
          index={index}
          onDrag={onMove}
          onOpenEdit={onOpenEdit}
          onDelete={onDelete}
        />
      ))}
    </div>
  );
};

export default PointStructureList;
