import * as React from 'react';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Grow from '@mui/material/Grow';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import MenuItem from '@mui/material/MenuItem';
import MenuList from '@mui/material/MenuList';
import Stack from '@mui/material/Stack';
import IconButton from '@mui/material/IconButton';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { IPointStructure } from '../../../models';

interface Props {
  id: string;
  pointStructure: IPointStructure;
  isClassCreator: boolean;
  onExportPointGrading: (pointStructure: any) => void;
  onToggleFinalizePoint: (pointStructure: any) => void;
  onOpenRequestGradeReview: (pointStructure: any) => void;
}

const PointStructureColOption: React.FC<Props> = (props) => {
  const {
    id,
    pointStructure,
    isClassCreator,
    onExportPointGrading,
    onToggleFinalizePoint,
    onOpenRequestGradeReview,
  } = props;

  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef<any>(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event: any) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event: any) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    } else if (event.key === 'Escape') {
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  const exportPointGrading = (e: any) => {
    onExportPointGrading(pointStructure);
    handleClose(e);
  };

  const toggleFinalizePoint = (e: any) => {
    onToggleFinalizePoint(pointStructure);
    handleClose(e);
  };

  const openRequestGradeReview = (e: any) => {
    onOpenRequestGradeReview(pointStructure);
    handleClose(e);
  };

  return (
    <Stack direction="row" spacing={2}>
      <div>
        <IconButton
          ref={anchorRef}
          aria-label="more"
          id={id}
          aria-controls={open ? 'composition-menu' : undefined}
          aria-expanded={open ? 'true' : undefined}
          aria-haspopup="true"
          onClick={handleToggle}
        >
          <MoreVertIcon />
        </IconButton>
        <Popper
          open={open}
          anchorEl={anchorRef.current}
          role={undefined}
          placement="bottom-end"
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin:
                  placement === 'bottom-start' ? 'left top' : 'left bottom',
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList
                    autoFocusItem={open}
                    id="composition-menu"
                    aria-labelledby={id}
                    onKeyDown={handleListKeyDown}
                  >
                    {isClassCreator ? (
                      <>
                        <MenuItem onClick={exportPointGrading}>
                          Export grading
                        </MenuItem>
                        <MenuItem onClick={toggleFinalizePoint}>
                          {pointStructure.isFinalized
                            ? 'Unfinalize'
                            : 'Finalize'}
                        </MenuItem>
                      </>
                    ) : null}

                    {!isClassCreator ? (
                      <MenuItem onClick={openRequestGradeReview}>
                        Request grade review...
                      </MenuItem>
                    ) : null}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    </Stack>
  );
};

export default PointStructureColOption;
