import React, { ReactNode, useEffect, useState } from 'react';
import Img from 'next/image';
import { IconButton } from '@mui/material';
import RateReviewIcon from '@mui/icons-material/RateReview';
import userLogo from '/public/user.png';
import PointStructureColOption from './pointStructureOption';
import { IPointStructure } from '../../../models';
import GradeCommentPopup from '../modal/gradeCommentPopup';
import GradeReviewPopup from '../modal/gradeReviewPopup';
import { useAppSelector } from '../../../redux/store';
import styles from './index.module.scss';

interface Props {
  gradeBoard: any;
  isClassCreator: boolean;
  onUpdateMarkOnBoard: (data: any) => void;
  onExportGradingForPointStructure: (pointStructure: any) => void;
  onToggleFinalForPointStructure: (pointStructure: any) => void;
  onRequestReviewForGrading: (pointStructure: any) => void;
  onCommentGradingReview: (data: any) => void;
  onApproveReviewForGrading: (data: any) => void;
}

const GradeBoard: React.FC<Props> = (props: any) => {
  const {
    gradeBoard,
    isClassCreator,
    onUpdateMarkOnBoard,
    onExportGradingForPointStructure,
    onToggleFinalForPointStructure,
    onRequestReviewForGrading,
    onCommentGradingReview,
    onApproveReviewForGrading,
  } = props;

  const authInfo = useAppSelector((state) => state.authInfo);

  const [selectedStudentForComment, setSelectedStudentForComment] =
    useState<any>({});

  useEffect(() => {
    if (!gradeBoard?.data) return;

    const newSelectedStudentForComment = gradeBoard.data.students.find(
      (student: any) => student.user?.id === selectedStudentForComment.user?.id
    );

    if (newSelectedStudentForComment) {
      setSelectedStudentForComment(newSelectedStudentForComment);

      setGradeCommentPopupData({
        pointStructure: gradeBoard.data?.pointStructure,
        student: newSelectedStudentForComment,
      });
    }
  }, [gradeBoard]);

  const [gradeReviewPopupData, setGradeReviewPopupData] = useState<any>({});
  const [gradeReviewPopupOpen, setGradeReviewPopupOpen] =
    useState<boolean>(false);

  const [gradeCommentPopupOpen, setGradeCommentPopupOpen] =
    useState<boolean>(false);
  const [gradeCommentPopupData, setGradeCommentPopupData] = useState<any>({});

  const markClick = (e: any) => {
    if (!isClassCreator) return;
    e.target.classList.add(`${styles['mark--hide']}`);
    e.target.nextSibling.classList.remove(`${styles['input--hide']}`);
  };

  const markInputClose = (e: any, initialMark: number) => {
    e.target.previousSibling.previousSibling.value = initialMark;
    e.target.parentElement.previousSibling.classList.remove(
      `${styles['mark--hide']}`
    );
    e.target.parentElement.classList.add(`${styles['input--hide']}`);
  };

  const updateMark = async (e: any, studentId: string, markCode: string) => {
    if (!isClassCreator) return;
    const newMark = e.target.previousSibling.value;
    e.target.parentElement.previousSibling.classList.remove(
      `${styles['mark--hide']}`
    );
    e.target.parentElement.classList.add(`${styles['input--hide']}`);
    const markUpdateBody = {
      studentId,
      code: markCode,
      point: +newMark,
    };
    onUpdateMarkOnBoard(markUpdateBody);
  };

  const onExportPointGrading = (pointStructure: any) => {
    onExportGradingForPointStructure(pointStructure);
  };

  const onToggleFinalizePoint = (pointStructure: any) => {
    onToggleFinalForPointStructure(pointStructure);
  };

  const onOpenRequestGradeReview = (pointStructure: any) => {
    setGradeReviewPopupData(pointStructure);
    setGradeReviewPopupOpen(true);
  };

  const openGradeCommentPopup = (student: any) => {
    setSelectedStudentForComment(student);

    setGradeCommentPopupData({
      pointStructure: gradeBoard?.data?.pointStructure,
      student,
    });
    setGradeCommentPopupOpen(true);
  };

  const onRequestGradeReview = (data: any) => {
    onRequestReviewForGrading(data);
    setGradeReviewPopupOpen(false);
  };

  const onCommentGradeReview = (data: any) => {
    onCommentGradingReview(data);
  };

  const onApproveGradeReview = (data: any) => {
    onApproveReviewForGrading(data);
  };

  const renderTable = () => {
    if (!gradeBoard?.data) return <></>;

    const tableContent: ReactNode[] = [];
    const pointStructures: IPointStructure[] = gradeBoard.data.pointStructure;

    tableContent.push(
      <colgroup key="grade-group-1">
        <col span={1} style={{ width: '20%' }} />
        {pointStructures.length > 0 ? (
          pointStructures.map((_: any, index: number) => (
            <col key={`col-group-${index + 1}`} span={1} />
          ))
        ) : (
          <col span={1} />
        )}
        <col span={1} style={{ width: '10%' }} />
      </colgroup>
    );

    tableContent.push(
      <thead key="grade-group-2">
        <tr>
          <th>Student Info</th>
          {pointStructures.length > 0 ? (
            pointStructures.map((item: IPointStructure, index: number) => (
              <th key={`point-structure-${index + 1}`}>
                {item.name}
                <span className={styles['text-small']}>
                  {item.point} points
                </span>
                <div className={styles.option}>
                  <PointStructureColOption
                    id={`point-structure-option-${index + 1}`}
                    isClassCreator={isClassCreator}
                    pointStructure={item}
                    onExportPointGrading={onExportPointGrading}
                    onToggleFinalizePoint={onToggleFinalizePoint}
                    onOpenRequestGradeReview={onOpenRequestGradeReview}
                  />
                </div>
              </th>
            ))
          ) : (
            <th>Class has no points structures</th>
          )}
          <th>Total Grade</th>
        </tr>
      </thead>
    );

    const rows: ReactNode[] = [];
    for (let i = 0; i < gradeBoard.data.students.length; i++) {
      const student = gradeBoard.data.students[i];
      rows.push(
        <tr key={`grade-group-3-${i}`}>
          <td>
            {student.user ? (
              <div className={styles.user}>
                <div>
                  <Img
                    src={student.user?.avatarUrl || userLogo}
                    data-src={student.user?.avatarUrl || userLogo}
                    data-srcset={student.user?.avatarUrl || userLogo}
                    alt="avatar"
                    width={40}
                    height={40}
                  />
                </div>
                <div>
                  {student.name}
                  {student.user?.studentId ? (
                    <span className={styles['text-small']}>
                      Id: {student.user.studentId}
                    </span>
                  ) : (
                    <></>
                  )}
                </div>
                {student.user?.id === authInfo.id || isClassCreator ? (
                  <div className={styles['user__grade-review']}>
                    <IconButton
                      style={{ width: 'fit-content', height: 'fit-content' }}
                      onClick={() => openGradeCommentPopup(student)}
                    >
                      <RateReviewIcon />
                    </IconButton>
                  </div>
                ) : null}
              </div>
            ) : (
              <>{student.name}</>
            )}
          </td>
          {student.grade.length > 0 ? (
            student.grade.map((item: any, index: number) => (
              <td key={`student-${i + 1}-point-structure-${index + 1}`}>
                <span
                  className={`${styles.mark} ${
                    isClassCreator ? styles['mark--cursor'] : ''
                  }`}
                  onClick={markClick}
                >
                  {item.mark < 0 ? 'No mark' : item.mark}
                </span>
                <div className={`${styles.input} ${styles['input--hide']}`}>
                  <input
                    className={styles.input__input}
                    defaultValue={item.mark}
                    type="number"
                  />
                  <button
                    className={styles.input__button}
                    onClick={(e) =>
                      updateMark(
                        e,
                        student.user?.studentId || student.studentId,
                        item.code
                      )
                    }
                  >
                    &#10003;
                  </button>
                  <button
                    className={`${styles.input__button} ${styles['input__button--red']}`}
                    onClick={(e: any) => markInputClose(e, item.mark)}
                  >
                    &#10006;
                  </button>
                </div>
              </td>
            ))
          ) : (
            <td></td>
          )}
          <td>{student.totalGrade}</td>
        </tr>
      );
    }

    tableContent.push(<tbody key="grade-group-4">{rows}</tbody>);

    return <table>{tableContent}</table>;
  };

  return (
    <>
      <div className={styles.board}>{renderTable()}</div>

      <GradeReviewPopup
        data={gradeReviewPopupData}
        open={gradeReviewPopupOpen}
        error=""
        handleClose={() => setGradeReviewPopupOpen(false)}
        onRequestGradeReview={onRequestGradeReview}
      />

      <GradeCommentPopup
        data={gradeCommentPopupData}
        open={gradeCommentPopupOpen}
        isClassCreator={isClassCreator}
        onCommentGradeReview={onCommentGradeReview}
        onApproveGradeReview={onApproveGradeReview}
        handleClose={() => setGradeCommentPopupOpen(false)}
      />
    </>
  );
};

export default GradeBoard;
