import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

const StyledMenu = styled((props: any) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === 'light'
        ? 'rgb(55, 65, 81)'
        : theme.palette.grey[300],
    boxShadow:
      'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
    '& .MuiMenu-list': {
      padding: '4px 0',
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      '&:active': {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        ),
      },
    },
  },
}));

const GradingOption = (props: any) => {
  const {
    onDownloadStudentListTemplate,
    onUploadStudentList,
    onUploadGrading,
    onDownloadGradingBoard,
  } = props;

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onClickDownloadStudentListTemplate = () => {
    onDownloadStudentListTemplate();
    handleClose();
  };

  const onClickUploadStudentListTemplate = () => {
    let input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute(
      'accept',
      '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'
    );
    input.onchange = (e: any) => {
      const file: any = e.target?.files[0];
      if (!file) return;
      onUploadStudentList(file);
    };
    input.click();
    input.remove();

    handleClose();
  };

  const onClickUploadGrading = () => {
    let input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute(
      'accept',
      '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'
    );
    input.onchange = (e: any) => {
      const file: any = e.target?.files[0];
      if (!file) return;
      onUploadGrading(file);
    };
    input.click();
    input.remove();

    handleClose();
  };

  const onClickDownloadGradingBoard = () => {
    onDownloadGradingBoard();
    handleClose();
  };

  return (
    <div>
      <Button
        id="demo-customized-button"
        aria-controls="demo-customized-menu"
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        variant="contained"
        disableElevation
        onClick={handleClick}
        endIcon={<KeyboardArrowDownIcon />}
      >
        Grading Option
      </Button>
      <StyledMenu
        id="demo-customized-menu"
        MenuListProps={{
          'aria-labelledby': 'demo-customized-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={onClickDownloadStudentListTemplate} disableRipple>
          Download student list template
        </MenuItem>
        <MenuItem onClick={onClickUploadStudentListTemplate} disableRipple>
          Upload student list
        </MenuItem>
        <MenuItem onClick={onClickUploadGrading} disableRipple>
          Upload grading
        </MenuItem>
        <MenuItem onClick={onClickDownloadGradingBoard} disableRipple>
          Export grading board
        </MenuItem>
      </StyledMenu>
    </div>
  );
};

export default GradingOption;
