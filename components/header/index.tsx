import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { Menu, MenuItem, Divider } from '@mui/material';
import logo from '../../public/logo.png';
import userImage from '../../public/user.png';
import { useAppSelector, useAppDispatch } from '../../redux/store';
import { signOut } from '../../redux/slices/auth';
import AppSelect from '../appSelect';
import NotificationRing from '../notification/list';
import styles from './index.module.scss';

function Header() {
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.authInfo);
  const classState = useAppSelector((state) => state.class);

  const router = useRouter();

  const [classFound, setClassFound] = useState<boolean>(false);
  const [menuAnchor, setMenuAnchor] = useState(null);
  const menuOpen = Boolean(menuAnchor);

  useEffect(() => {
    if (router.pathname.startsWith('/class') && classState.class.id !== '')
      setClassFound(true);
  }, [classState.class.id]);

  const handleClick = (event: any) => {
    setMenuAnchor(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setMenuAnchor(null);
  };

  const logout = () => {
    dispatch(signOut());
    localStorage.clear();
  };

  return (
    <div className={styles.container}>
      <div
        className={styles.section}
        onClick={() => {
          router.push({ pathname: '/' });
        }}
      >
        <Image alt="CLASSROOM" src={logo} width={50} height={50} />
        <span className={styles.logoText}>MY CLASSROOM</span>
      </div>
      <div className={styles.section}>
        <AppSelect classFound={classFound} />

        <NotificationRing />

        <Image
          className={`${styles.profileImage} profile-image`}
          onClick={handleClick}
          alt="user"
          src={user.avatarUrl || userImage}
          width={40}
          height={40}
        />

        <Menu anchorEl={menuAnchor} open={menuOpen} onClose={handleCloseMenu}>
          <div className={styles.username}>
            <span>
              {user.firstName} {user.lastName}
            </span>
          </div>
          <Divider />
          <MenuItem onClick={() => router.push('/profile/me')}>
            Profile
          </MenuItem>
          <MenuItem onClick={logout}>Logout</MenuItem>
        </Menu>
      </div>
    </div>
  );
}

export default Header;
