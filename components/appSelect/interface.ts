export interface AppModalItem {
  icon: any;
  text: string;
  onClick: () => void;
}

export interface AppLinkItem {
  icon: any;
  text: string;
  linkTo: (id: string | number) => string;
  recognition: string;
}
