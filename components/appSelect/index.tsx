import React, { useRef, useState } from 'react';
import AppsIcon from '@mui/icons-material/Apps';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import FeedIcon from '@mui/icons-material/FeedOutlined';
import PeopleIcon from '@mui/icons-material/People';
import InfoIcon from '@mui/icons-material/InfoOutlined';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import ListAltIcon from '@mui/icons-material/ListAlt';
import GradingIcon from '@mui/icons-material/Grading';
import JoinClassIcon from '@mui/icons-material/ReadMore';
import AppBox from './appBox';
import JoinClassModal from '../class/modal/join';
import CreateClassModal from '../class/modal/create';
import { useAppDispatch } from '../../redux/store';
import classApi from '../../api/classApi';
import { getAllClass } from '../../redux/slices/class';
import { AppLinkItem, AppModalItem } from './interface';
import {
  showErrorNoti,
  showSuccessNoti,
} from '../../redux/slices/notification';
import styles from './index.module.scss';

const AppSelect: React.FC<{ classFound: boolean }> = (props) => {
  const { classFound = false } = props;

  const dispatch = useAppDispatch();

  const [appBoxOpen, setAppBoxOpen] = useState<boolean>(false);
  const toggleButtonRef = useRef(null);

  const [joinModalOpen, setJoinModalOpen] = useState<boolean>(false);
  const [createModalOpen, setCreateModalOpen] = useState<boolean>(false);
  const [joinError, setJoinError] = useState<string>('');

  const toggleAppBox = (event: any) => {
    setAppBoxOpen((prev) => !prev);
  };

  const joinClass = async (data: any) => {
    try {
      const res = await classApi.joinByCode(data);
      if (res) {
        setJoinModalOpen(false);
        dispatch(getAllClass());
        dispatch(showSuccessNoti('Joined class!'));
        return;
      }
      dispatch(showErrorNoti('Error joining class'));
    } catch (error: any) {
      setJoinError(JSON.parse(error.message).message);
      dispatch(showErrorNoti(JSON.parse(error.message).message));
    }
  };

  const createClass = async (data: any) => {
    try {
      setCreateModalOpen(false);
      const res = await classApi.createOne(data);
      if (res) {
        dispatch(getAllClass());
        dispatch(showSuccessNoti('Class created!'));
        return;
      }
      dispatch(showErrorNoti('Error creating class'));
    } catch (error: any) {
      console.log(JSON.parse(error.message).message);
      dispatch(showErrorNoti(JSON.parse(error.message).message));
    }
  };

  const appModals: AppModalItem[] = [
    {
      icon: <JoinClassIcon />,
      text: 'Join class',
      onClick: () => setJoinModalOpen(true),
    },
    {
      icon: <AddCircleIcon />,
      text: 'Create class',
      onClick: () => setCreateModalOpen(true),
    },
  ];

  const classNavigations: AppLinkItem[] = [
    {
      icon: <FeedIcon />,
      text: 'Posts',
      linkTo: (id: string | number) => `/class/${id}/home`,
      recognition: '/home',
    },
    {
      icon: <PeopleIcon />,
      text: 'People',
      linkTo: (id: string | number) => `/class/${id}/people`,
      recognition: '/people',
    },
    {
      icon: <InfoIcon />,
      text: 'About',
      linkTo: (id: string | number) => `/class/${id}/about`,
      recognition: '/about',
    },
    {
      icon: <ListAltIcon />,
      text: 'Points',
      linkTo: (id: string | number) => `/class/${id}/point-structure`,
      recognition: '/point-structure',
    },
    {
      icon: <GradingIcon />,
      text: 'Grades',
      linkTo: (id: string | number) => `/class/${id}/grading`,
      recognition: '/grading',
    },
  ];

  return (
    <>
      <div className={styles.app}>
        <div
          className={styles.app__icon}
          onClick={toggleAppBox}
          ref={toggleButtonRef}
        >
          <Tooltip title="App">
            <IconButton>
              <AppsIcon />
            </IconButton>
          </Tooltip>
        </div>

        <AppBox
          appModals={appModals}
          classNavigations={classFound ? classNavigations : []}
          open={appBoxOpen}
          onClose={() => setAppBoxOpen(false)}
          toggleButtonRef={toggleButtonRef}
        />
      </div>

      <JoinClassModal
        open={joinModalOpen}
        handleClose={() => {
          setJoinModalOpen(false);
          setJoinError('');
        }}
        clickJoin={joinClass}
        error={joinError}
        clearError={() => setJoinError('')}
      />

      <CreateClassModal
        open={createModalOpen}
        handleClose={() => setCreateModalOpen(false)}
        clickCreate={createClass}
      />
    </>
  );
};

export default AppSelect;
