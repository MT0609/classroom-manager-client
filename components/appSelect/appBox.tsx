import React, { useRef } from 'react';
import { useRouter } from 'next/router';
import { Grid, Divider } from '@mui/material';
import OnClickOutside from '../../hooks/onClickOutside';
import { useAppSelector } from '../../redux/store';
import { AppLinkItem, AppModalItem } from './interface';
import styles from './index.module.scss';
import { checkIsCreator } from '../../utils';

interface Props {
  open: boolean;
  appModals: AppModalItem[];
  classNavigations: AppLinkItem[];
  onClose: () => void;
  toggleButtonRef: any;
}

const AppBox: React.FC<Props> = (props) => {
  const classState = useAppSelector((state) => state.class);

  const appBoxRef = useRef(null);
  const router = useRouter();

  const {
    open = false,
    classNavigations,
    appModals,
    toggleButtonRef,
    onClose,
  } = props;

  OnClickOutside(appBoxRef, () => onClose(), toggleButtonRef);

  return (
    <div
      ref={open ? appBoxRef : null}
      className={`${!open ? styles['app__box--close'] : styles['app__box']}`}
    >
      <div className={styles['app__section']}>
        {appModals.map((item, index) => (
          <div
            key={index}
            className={styles.app__item}
            onClick={() => {
              onClose();
              item.onClick();
            }}
          >
            {item.icon}
            <p>{item.text}</p>
          </div>
        ))}
      </div>

      {classNavigations.length ? (
        <>
          <Divider />
          <p className={styles['app__section__title']}>
            Class: {classState.class.name}
          </p>
          <div className={styles['app__section']}>
            <Grid container spacing={2}>
              {classNavigations.map((item, index) => {
                const { recognition } = item;
                const active = router.pathname.endsWith(recognition);
                const isClassCreator = checkIsCreator(
                  classState.class.createdUser?.id
                );
                if (!isClassCreator && recognition === '/point-structure')
                  return <></>;

                return (
                  <Grid item xs={4} key={`class-navigation-${index}`}>
                    <div
                      className={`${styles.app__item} ${
                        active ? styles['app__item--active'] : ''
                      }`}
                      onClick={() =>
                        router.push(item.linkTo(classState.class.id))
                      }
                    >
                      {item.icon}
                      <p>{item.text}</p>
                    </div>
                  </Grid>
                );
              })}
            </Grid>
          </div>
        </>
      ) : (
        <></>
      )}
    </div>
  );
};

export default AppBox;
