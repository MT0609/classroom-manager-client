import React from 'react';
import { Box } from '@mui/material';
import ClassCardLoading from '../../class/classCard/skeletons';
import Header from '../../header';
import LoadingWheel from '../../loading/wheel';
import styles from './index.module.scss';

interface Props {
  isLoading: boolean;
  loadingType?: string;
  padding?: boolean;
}

const MainTemplate: React.FC<Props> = (props) => {
  const { isLoading = true, children, padding = true } = props;

  return (
    <>
      <div className={styles.header}>
        <Header />
      </div>
      <div
        className={`${styles.body} ${padding ? styles['body--padding'] : ''}`}
      >
        {isLoading ? (
          props.loadingType === 'card' ? (
            <ClassCardLoading />
          ) : (
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                p: 5,
              }}
            >
              <LoadingWheel />
            </Box>
          )
        ) : (
          children
        )}
      </div>
    </>
  );
};

export default MainTemplate;
