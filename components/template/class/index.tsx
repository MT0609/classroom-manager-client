import React, { useMemo } from 'react';
import { useRouter } from 'next/router';
import Img from 'next/image';
import Link from 'next/link';
import { Divider } from '@mui/material';
import MainTemplate from '../main';
import classroomBackground from '/public/home-classroom-background.jpg';
import { IClassroom } from '../../../models';
import styles from './index.module.scss';

interface Props {
  classInfo: IClassroom;
}

interface LinkItem {
  text: string;
  linkTo: (id: string | number) => string;
  recognition: string;
}

const ClassroomTemplate: React.FC<Props> = (props) => {
  const router = useRouter();
  const { classInfo } = props;

  const links: LinkItem[] = useMemo(
    () => [
      {
        text: 'Posts',
        linkTo: (id: string | number) => `/class/${id}/home`,
        recognition: '/home',
      },
      {
        text: 'People',
        linkTo: (id: string | number) => `/class/${id}/people`,
        recognition: '/people',
      },
      {
        text: 'Grade',
        linkTo: (id: string | number) => `/class/${id}/grading`,
        recognition: '/grading',
      },
      {
        text: 'About',
        linkTo: (id: string | number) => `/class/${id}/about`,
        recognition: '/about',
      },
    ],
    []
  );

  return (
    <MainTemplate isLoading={false} padding={false}>
      {classInfo.id ? (
        <>
          <div className={styles.top}>
            <div>
              <Img src={classroomBackground} alt="classroom-background" />
              <p className={styles.name}>{classInfo.name}</p>
              <Divider variant="fullWidth" />
              <ul className={styles.menu}>
                {links.map((item: LinkItem, index: number) => {
                  const { recognition } = item;
                  const active = router.pathname.endsWith(recognition);

                  return (
                    <li
                      key={index}
                      className={`${styles.menu__item} ${
                        active ? styles['menu__item--active'] : ''
                      }`}
                      onClick={() => router.push(item.linkTo(classInfo.id))}
                    >
                      <Link href={`${item.linkTo(classInfo.id)}`}>
                        <a>{item.text}</a>
                      </Link>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>

          <div className={styles.content}>{props.children}</div>
        </>
      ) : (
        <p>Class not found</p>
      )}
    </MainTemplate>
  );
};

export default ClassroomTemplate;
