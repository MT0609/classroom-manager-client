import React, { useRef } from 'react';
import Link from 'next/link';
import OnClickOutside from '../../../hooks/onClickOutside';
import NotificationItem from './notificationItem';
import { ReferenceType } from '../../../models';
import styles from './index.module.scss';
import { Divider } from '@mui/material';

interface Props {
  notiData: any;
  open: boolean;
  loading: boolean;
  onClose: () => void;
  toggleButtonRef: any;
}

const NotificationList: React.FC<Props> = (props) => {
  const appBoxRef = useRef(null);

  const {
    loading = false,
    open = false,
    notiData,
    toggleButtonRef,
    onClose,
  } = props;

  OnClickOutside(appBoxRef, () => onClose(), toggleButtonRef);

  if (!notiData?.data) return <></>;

  return (
    <div
      ref={open ? appBoxRef : null}
      className={`${!open ? styles['app__box--close'] : styles['app__box']}`}
    >
      {loading ? <p>Loading...</p> : <></>}

      {notiData.data.map((item: any, index: number) => {
        const image = {
          source: '/message.png',
          alt: 'noti',
        };
        let linkTo = '';
        switch (item.type) {
          case ReferenceType.FINALIZE_COLUMN:
            image.source = '/grading.png';
            image.alt = 'grading';
            linkTo = `/class/${item.metadata.classId}/grading`;
            break;
          case ReferenceType.REQUEST_GRADE_VIEW:
            image.source = '/grade-review.png';
            image.alt = 'grade-review';
            linkTo = `/class/${item.metadata.classId}/grading`;
            break;
          case ReferenceType.REPLY_GRADE_VIEW:
            linkTo = `/class/${item.metadata.classId}/grading`;
            break;
          default:
            linkTo = `/`;
            break;
        }

        return (
          <div key={`noti-${index}`}>
            <Link href={linkTo} passHref>
              <NotificationItem image={image} data={item} />
            </Link>
            {index < notiData.data.length - 1 ? (
              <div className={styles.notification__divider}>
                <Divider />
              </div>
            ) : (
              <></>
            )}
          </div>
        );
      })}
    </div>
  );
};

export default NotificationList;
