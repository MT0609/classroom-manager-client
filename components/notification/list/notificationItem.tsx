import React from 'react';
import Img from 'next/image';
import styles from './index.module.scss';

interface Props extends React.HTMLProps<HTMLAnchorElement> {
  image: any;
  data: any;
}

const NotificationItem = React.forwardRef(function Component(
  props: Props,
  ref: any
) {
  const { image, data, onClick, href } = props;

  return (
    <a href={href || '#'} onClick={onClick} ref={ref}>
      <div className={styles.notification__item}>
        <div className={styles.notification__image}>
          <Img src={image.source} width={50} height={50} alt={image.alt} />
        </div>

        <div className={styles.notification__item__right}>
          <p className={styles.notification__title}>{data.title}</p>
          <p>{data.content}</p>
          <p className={styles.notification__time}>
            {new Date(data.updatedAt).toLocaleString()}
          </p>
        </div>
      </div>
    </a>
  );
});

export default NotificationItem;
