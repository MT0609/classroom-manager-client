import React, { useRef, useState } from 'react';
import NotificationsIcon from '@mui/icons-material/Notifications';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import NotificationList from './notificationList';
import { useAppSelector, useAppDispatch } from '../../../redux/store';
import { getUserNotifications } from '../../../redux/slices/notificationList';
import styles from './index.module.scss';

const NotificationRing = () => {
  const userNotifications = useAppSelector((state) => state.notificationList);
  const notifications = userNotifications.notifications;
  const isLoading = userNotifications.isLoading;

  const dispatch = useAppDispatch();

  const [notificationListOpen, setNotificationListOpen] =
    useState<boolean>(false);
  const toggleButtonRef = useRef(null);

  const toggleAppBox = (event: any) => {
    setNotificationListOpen((prev) => !prev);
    if (!notificationListOpen) dispatch(getUserNotifications());
  };

  return (
    <>
      <div className={styles.app}>
        <div
          className={styles.app__icon}
          onClick={toggleAppBox}
          ref={toggleButtonRef}
        >
          <Tooltip title="App">
            <IconButton>
              <NotificationsIcon />
            </IconButton>
          </Tooltip>
        </div>

        <NotificationList
          loading={isLoading}
          notiData={notifications}
          open={notificationListOpen}
          onClose={() => setNotificationListOpen(false)}
          toggleButtonRef={toggleButtonRef}
        />
      </div>
    </>
  );
};

export default NotificationRing;
