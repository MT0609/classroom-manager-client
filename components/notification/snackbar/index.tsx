import * as React from 'react';
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import { useAppDispatch, useAppSelector } from '../../../redux/store';
import { closeNotification } from '../../../redux/slices/notification';

const Alert = React.forwardRef(function Alert(props: any, ref: any) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function NotiSnackbar() {
  const dispatch = useAppDispatch();

  const { open, message, state } = useAppSelector(
    (state) => state.notification
  );

  const handleClose = (event: any, reason: any) => {
    if (reason === 'clickaway') return;

    dispatch(closeNotification());
  };

  return (
    <Stack spacing={2} sx={{ width: '100%' }}>
      <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={state} sx={{ width: '100%' }}>
          {message}
        </Alert>
      </Snackbar>
    </Stack>
  );
}
