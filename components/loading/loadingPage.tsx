import type { NextPage } from 'next';
import Head from 'next/head';
import React from 'react';
import Loading from './wheel';
import styles from './index.module.scss';

const LoadingPage: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Login | Register | Classroom</title>
        <meta
          name="description"
          content="Login to Classroom; Register to Classroom; My Classroom - connect for futures"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.wheel}>
        <Loading />
      </div>
      <span className={styles.text}>Loading...</span>
    </div>
  );
};

export default LoadingPage;
