import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { firebase } from '../../config/firebase';
import OnSocialAuth from '../../hooks/onSocialAuth';
import styles from './index.module.scss';

const uiConfig = {
  signInFlow: 'popup',
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.FacebookAuthProvider.PROVIDER_ID,
  ],
  callbacks: {
    signInSuccessWithAuthResult: function (a: any, redirectUrl: any) {
      console.log(a, redirectUrl);
      return false;
    },
    signInFailure: function (error: any) {
      console.log(error.message);
      return Promise.resolve();
    },
  },
};

const SocialLogin: React.FC = () => {
  OnSocialAuth();

  return (
    <div>
      <p className={styles.text}>Or sign in with</p>
      <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
    </div>
  );
};

export default SocialLogin;
