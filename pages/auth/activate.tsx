import type { NextPage } from 'next';
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Img from 'next/image';
import { useRouter } from 'next/router';
import Box from '@mui/material/Box';
import ErrorIcon from '@mui/icons-material/Error';
import { CircularProgress } from '@mui/material';
import UserImage from '/public/user.png';
import { useAppDispatch } from '../../redux/store';
import accountApi from '../../api/accountApi';
import {
  showErrorNoti,
  showSuccessNoti,
} from '../../redux/slices/notification';
import styles from './index.module.scss';

const ActivateAccountPage: NextPage = () => {
  const router = useRouter();

  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string>('');

  const activateAccount = async (token: string) => {
    try {
      setLoading(true);
      const res: any = await accountApi.activateAccount({ token });
      if (res) {
        dispatch(showSuccessNoti(res.msg));
        router.replace('/auth/login');
        return;
      }
    } catch (error: any) {
      const err = JSON.parse(error.message).message;
      console.log(err || error.message);
      setLoading(false);
      setError(err || 'Activate account failed');
      dispatch(showErrorNoti(JSON.parse(error.message).message));
    }
  };

  useEffect(() => {
    const search = window.location.search;
    const params = new URLSearchParams(search);
    const token = params.get('token');

    if (token) activateAccount(token);
    else {
      setTimeout(() => {
        router.replace('/auth/login');
      }, 100);
    }
  }, []);

  return (
    <div className={styles.container}>
      <Head>
        <title>Activate Account | Classroom</title>
      </Head>

      <div className={styles.main}>
        <div className={styles.main__image}>
          <Img src={UserImage} alt="classroom" width={200} height={200} />
        </div>

        {loading ? (
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              p: 1,
              m: 1,
            }}
          >
            <CircularProgress size={20} style={{ marginRight: '5px' }} />
            <p>Please wait while activating your account</p>
          </Box>
        ) : (
          <></>
        )}

        {error ? (
          <>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                p: 1,
                m: 1,
              }}
            >
              <p className="error">{error}</p>
            </Box>
          </>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
};

export default ActivateAccountPage;
