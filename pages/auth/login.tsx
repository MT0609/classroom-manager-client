import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { TextField } from '@mui/material';
import accountApi from '../../api/accountApi';
import LoadingButton from '../../components/button/loadingButton';
import { useAppDispatch } from '../../redux/store';
import { IFormLogin } from '../../models/form';
import { storeAuthInfo } from '../../redux/slices/auth';
import SocialLogin from '../../components/socialLogin';
import OnAutoRefresh from '../../hooks/onAutoRefresh';
import styles from './index.module.scss';

const LoginPage: NextPage = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormLogin>();

  const dispatch = useAppDispatch();

  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState('');

  const onSubmit = async (data: IFormLogin) => {
    try {
      setLoading(true);

      const result: any = await accountApi.signIn(data);

      if (result) {
        setLoading(false);
        dispatch(storeAuthInfo(result.user));
        return;
      }
      setError('Email or password is not correct');
      setLoading(false);
    } catch (error: any) {
      console.log(error.message);
      setError(JSON.parse(error.message).message);
      setLoading(false);
    }
  };

  OnAutoRefresh();

  return (
    <div className={styles.container}>
      <Head>
        <title>Login | Classroom</title>
        <meta
          name="description"
          content="Login to Classroom; My Classroom - connect for futures"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.main}>
        <form onSubmit={handleSubmit(onSubmit)} onChange={() => setError('')}>
          <span className={styles.title}>Login</span>

          <div className={styles.form}>
            <div className={styles.input}>
              <TextField
                required
                label="Email"
                type="email"
                fullWidth
                {...register('email', { required: 'Email is required' })}
                error={Boolean(errors.email)}
                helperText={errors.email?.message}
              />
            </div>

            <div className={styles.input}>
              <TextField
                required
                label="Password"
                type="password"
                fullWidth
                {...register('password', { required: 'Password is required' })}
                error={Boolean(errors.password)}
                helperText={errors.password?.message}
              />
            </div>
          </div>

          {error && <span className="error">{error}</span>}

          <div className={styles.subText}>
            <Link href="/auth/register">
              <a className={styles.subText_text}>Not have an account</a>
            </Link>
            <br />
            <Link href="/auth/send-reset-mail">
              <a className={styles.subText_text}>Forgot password</a>
            </Link>
            <br />
            <Link href="/auth/resend-activate">
              <a className={styles.subText_text}>Resend activation</a>
            </Link>
          </div>

          <div className={styles.formBtns}>
            <LoadingButton
              variant="outlined"
              type="submit"
              title="LOG IN"
              loading={loading}
            />
          </div>

          <SocialLogin />
        </form>
      </div>
    </div>
  );
};

export default LoginPage;
