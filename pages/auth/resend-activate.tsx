import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { TextField } from '@mui/material';
import accountApi from '../../api/accountApi';
import LoadingButton from '../../components/button/loadingButton';
import { useAppDispatch } from '../../redux/store';
import styles from './index.module.scss';
import { showSuccessNoti } from '../../redux/slices/notification';

const ResendActivateAccountPage: NextPage = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<{ email: string }>();

  const dispatch = useAppDispatch();

  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState('');

  const onSubmit = async (data: { email: string }) => {
    try {
      setLoading(true);
      const result: any = await accountApi.resendActivateEmail(data);

      if (result) {
        dispatch(showSuccessNoti('Check your email for account activation'));
        return;
      }
    } catch (error: any) {
      console.log(error.message);
      setError(JSON.parse(error.message).message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>Resend activation email | Classroom</title>
        <meta
          name="description"
          content="Login to Classroom; My Classroom - connect for futures"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.main}>
        <form onSubmit={handleSubmit(onSubmit)} onChange={() => setError('')}>
          <span className={styles.title}>Activate Account</span>

          <div className={styles.form}>
            <div className={styles.input}>
              <TextField
                required
                label="Email"
                type="email"
                fullWidth
                {...register('email', { required: 'Email is required' })}
                error={Boolean(errors.email)}
                helperText={errors.email?.message}
              />
            </div>
          </div>

          {error && <span className="error">{error}</span>}

          <div className={styles.subText}>
            <Link href="/auth/login">
              <a className={styles.subText_text}>Login</a>
            </Link>
            <br />
            <Link href="/auth/register">
              <a className={styles.subText_text}>Not have an account</a>
            </Link>
          </div>

          <div className={styles.formBtns}>
            <LoadingButton
              variant="outlined"
              type="submit"
              title="Send activate link"
              loading={loading}
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default ResendActivateAccountPage;
