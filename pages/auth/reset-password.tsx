import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState, useRef, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { TextField } from '@mui/material';
import accountApi from '../../api/accountApi';
import LoadingButton from '../../components/button/loadingButton';
import { useAppDispatch } from '../../redux/store';
import { IResetPassword } from '../../models/form';
import styles from './index.module.scss';
import { showSuccessNoti } from '../../redux/slices/notification';

const ResetPasswordPage: NextPage = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm<IResetPassword>();

  const router = useRouter();
  const dispatch = useAppDispatch();

  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState('');

  const password = useRef({});
  password.current = watch('newPassword', '');

  const [token, setToken] = useState('');

  useEffect(() => {
    const search = window.location.search;
    const params = new URLSearchParams(search);
    const token = params.get('token');

    if (token) setToken(token);
    else {
      setTimeout(() => {
        router.replace('/auth/login');
      }, 100);
    }
  }, []);

  const onSubmit = async (data: IResetPassword) => {
    try {
      setLoading(true);

      const submitData = JSON.parse(JSON.stringify(data));
      delete submitData['confirmNewPassword'];
      submitData.token = token;

      const result: any = await accountApi.resetPassword(submitData);

      if (result) {
        dispatch(showSuccessNoti(result.msg));
        setLoading(false);
        router.push('/auth/login');
        return;
      }
    } catch (error: any) {
      console.log(error.message);
      setError(JSON.parse(error.message).message);
      setLoading(false);
    }
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>Reset password | Classroom</title>
        <meta
          name="description"
          content="Login to Classroom; My Classroom - connect for futures"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.main}>
        <form onSubmit={handleSubmit(onSubmit)} onChange={() => setError('')}>
          <span className={styles.title}>Reset password</span>

          <div className={styles.form}>
            <div className={styles.input}>
              <TextField
                required
                label="Password"
                type="password"
                fullWidth
                {...register('newPassword', {
                  required: 'Password is required',
                })}
                inputProps={{ minLength: 6, maxLength: 20 }}
                error={Boolean(errors.newPassword)}
                helperText={errors.newPassword?.message}
              />
            </div>

            <div className={styles.input}>
              <TextField
                required
                label="Confirm Password"
                type="password"
                fullWidth
                {...register('confirmNewPassword', {
                  validate: (value) =>
                    value === password.current || 'The passwords do not match',
                })}
                error={Boolean(errors.confirmNewPassword)}
                helperText={errors.confirmNewPassword?.message}
              />
            </div>
          </div>

          {error && <span className="error">{error}</span>}

          <div className={styles.subText}>
            <Link href="/auth/register">
              <a className={styles.subText_text}>Not have an account</a>
            </Link>
            <br />
            <Link href="/auth/resend-activate">
              <a className={styles.subText_text}>Resend activation</a>
            </Link>
          </div>

          <div className={styles.formBtns}>
            <LoadingButton
              variant="outlined"
              type="submit"
              title="Reset"
              loading={loading}
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default ResetPasswordPage;
