import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useRef, useState } from 'react';
import { TextField, Grid } from '@mui/material';
import { useForm } from 'react-hook-form';
import accountApi from '../../api/accountApi';
import LoadingButton from '../../components/button/loadingButton';
import { IFormRegister } from '../../models/form';
import SocialLogin from '../../components/socialLogin';
import { useAppDispatch } from '../../redux/store';
import OnAutoRefresh from '../../hooks/onAutoRefresh';
import styles from './index.module.scss';
import { showSuccessNoti } from '../../redux/slices/notification';

const Register: NextPage = () => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<IFormRegister>();

  const [loading, setLoading] = useState(false);
  const [noti, setNoti] = useState({
    type: '',
    msg: '',
  });

  const dispatch = useAppDispatch();
  const router = useRouter();

  const password = useRef({});
  password.current = watch('password', '');

  const onSubmit = async (data: IFormRegister) => {
    try {
      setLoading(true);
      setNoti({
        type: '',
        msg: '',
      });

      const submitData = JSON.parse(JSON.stringify(data));
      delete submitData['confirmPassword'];
      const result = await accountApi.signUp(submitData);

      if (result) {
        // setNoti({
        //   type: 'success',
        //   msg: 'Check your email for activation',
        // });
        setLoading(false);
        dispatch(showSuccessNoti('Check your email for activation'));
        router.push('/auth/login');
        return;
      }

      setLoading(false);
      setNoti({
        type: 'error',
        msg: 'Email is already taken',
      });
    } catch (error: any) {
      console.log(error.message);
      setLoading(false);
      setNoti({
        type: 'error',
        msg: JSON.parse(error.message).message,
      });
    }
  };

  OnAutoRefresh();

  return (
    <div className={styles.container}>
      <Head>
        <title>Register | Classroom</title>
        <meta
          name="description"
          content="Register Classroom; My Classroom - connect for futures"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.main}>
        <form
          onSubmit={handleSubmit(onSubmit)}
          onChange={() =>
            setNoti({
              type: '',
              msg: '',
            })
          }
        >
          <span className={styles.title}>Create Account</span>

          <div className={styles.form}>
            <div className={styles.input}>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <TextField
                    required
                    label="First Name"
                    {...register('firstName')}
                    size="small"
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    required
                    label="Last Name"
                    {...register('lastName')}
                    size="small"
                  />
                </Grid>
              </Grid>
            </div>

            <div className={styles.input}>
              <TextField
                required
                label="Email"
                type="email"
                fullWidth
                {...register('email', { required: 'Email is required' })}
                error={Boolean(errors.email)}
                helperText={errors.email?.message}
                size="small"
              />
            </div>

            <div className={styles.input}>
              <TextField
                required
                label="Password"
                type="password"
                fullWidth
                {...register('password', {
                  required: 'Password is required',
                })}
                inputProps={{ minLength: 6, maxLength: 20 }}
                error={Boolean(errors.password)}
                helperText={errors.password?.message}
                size="small"
              />
            </div>

            <div className={styles.input}>
              <TextField
                required
                label="Confirm Password"
                type="password"
                fullWidth
                {...register('confirmPassword', {
                  validate: (value) =>
                    value === password.current || 'The passwords do not match',
                })}
                error={Boolean(errors.confirmPassword)}
                helperText={errors.confirmPassword?.message}
                size="small"
              />
            </div>
          </div>

          {noti.msg && (
            <span
              className={`noti ${
                noti.type === 'success' ? 'success' : 'error'
              }`}
            >
              {noti.msg}
            </span>
          )}

          <div className={styles.subText}>
            <Link href="/auth/login">
              <a className={styles.subText_text}>I have an account</a>
            </Link>
            <br />
            <Link href="/auth/resend-activate">
              <a className={styles.subText_text}>Resend activation</a>
            </Link>
          </div>

          <div className={styles.formBtns}>
            <LoadingButton
              variant="outlined"
              type="submit"
              title="SIGN UP"
              loading={loading}
            />
          </div>

          <SocialLogin />
        </form>
      </div>
    </div>
  );
};

export default Register;
