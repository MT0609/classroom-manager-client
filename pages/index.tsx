import type { NextPage } from 'next';
import Head from 'next/head';
import React, { useEffect } from 'react';
import Img from 'next/image';
import ClassImage from '/public/class.png';
import ClassCard from '../components/class/classCard';
import MainTemplate from '../components/template/main';
import { getAllClass } from '../redux/slices/class';
import { useAppDispatch, useAppSelector } from '../redux/store';
import styles from '../styles/Home.module.scss';

const Home: NextPage = () => {
  const dispatch = useAppDispatch();

  const { classrooms, isLoading } = useAppSelector((state) => state.class);

  const getMyClassRooms = async () => {
    try {
      dispatch(getAllClass());
    } catch (error: any) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getMyClassRooms();
  }, []);

  return (
    <MainTemplate isLoading={isLoading} loadingType="card">
      <Head>
        <title>Home | Classroom</title>
        <meta name="description" content="My Classroom - connect for futures" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>
        <main className={styles.main}>
          {classrooms.data.length > 0 ? (
            <div className={styles.cards}>
              {classrooms.data.map((classroom, index: number) => (
                <div key={index} className={styles.card}>
                  <ClassCard classroom={classroom} />
                </div>
              ))}
            </div>
          ) : (
            <div className={styles['main--center']}>
              <Img src={ClassImage} alt="classroom" width={200} height={200} />
              <p>You have not joined or owned any classes</p>
            </div>
          )}
        </main>
      </div>
    </MainTemplate>
  );
};

export default Home;
