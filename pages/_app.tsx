import '../styles/globals.scss';
import { useEffect } from 'react';
import type { AppProps } from 'next/app';
import { ThemeProvider } from '@mui/material/styles';
import { getMyTheme } from '../config/theme';
import RouteGuard from '../hooks/routeGuard';
import AppNotiSnackbar from '../components/notification/snackbar';
import { resetTheme } from '../redux/slices/auth';
import { useAppDispatch, useAppSelector, wrapper } from '../redux/store';

function MyApp({ Component, pageProps }: AppProps) {
  const user = useAppSelector((state) => state.authInfo);
  const dispatch = useAppDispatch();

  useEffect(() => {
    const htmlRoot: any = document.querySelector(':root');

    if (!user.isAuthenticated) {
      if (!user.isFetching) {
        dispatch(resetTheme());
        return;
      }

      if (localStorage.getItem('theme')) {
        htmlRoot.setAttribute('theme', localStorage.getItem('theme'));
        return;
      }

      dispatch(resetTheme());
      htmlRoot.removeAttribute('theme');
      return;
    }

    htmlRoot.setAttribute('theme', user.theme);
    localStorage.setItem('theme', user.theme || 'light');
  }, [user.theme, user.isAuthenticated, user.isFetching]);

  return (
    <ThemeProvider theme={getMyTheme(user.theme || 'light')}>
      <RouteGuard>
        <Component {...pageProps} />
      </RouteGuard>

      <AppNotiSnackbar />
    </ThemeProvider>
  );
}
export default wrapper.withRedux(MyApp);
