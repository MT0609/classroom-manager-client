import type { NextPage } from 'next';
import React, { useState } from 'react';
import Head from 'next/head';
import MainTemplate from '../../components/template/main';
import ProfileTopSection from '../../components/profile/topSection';
import { useAppDispatch, useAppSelector } from '../../redux/store';
import AdditionProfileSection from '../../components/profile/additionSection';
import PasswordSection from '../../components/profile/passwordSection';
import accountApi from '../../api/accountApi';
import {
  showErrorNoti,
  showSuccessNoti,
} from '../../redux/slices/notification';
import { storeAuthInfo } from '../../redux/slices/auth';
import styles from './index.module.scss';
import { IChangePassword } from '../../models/form';

const ProfileSettingPage: NextPage = () => {
  const userState = useAppSelector((state) => state.authInfo);
  const dispatch = useAppDispatch();

  const [sectionsViewMode, setSectionsViewMode] = useState({
    top: true,
    middle: true,
    addition: true,
  });

  const topInfo = {
    avatar: userState.avatarUrl,
    firstName: userState.firstName,
    lastName: userState.lastName,
    studentId: userState.studentId,
    email: userState.email,
  };

  const additionalInfo = {
    theme: userState.theme,
  };

  const switchTopSectionMode = () => {
    setSectionsViewMode((prev: any) => ({
      ...prev,
      top: !sectionsViewMode.top,
    }));
  };

  const switchMiddleSectionMode = () => {
    setSectionsViewMode((prev: any) => ({
      ...prev,
      middle: !sectionsViewMode.middle,
    }));
  };

  const switchAdditionSectionMode = () => {
    setSectionsViewMode((prev: any) => ({
      ...prev,
      addition: !sectionsViewMode.addition,
    }));
  };

  const updateProfile = async (obj: any) => {
    try {
      const updateBody = {
        firstName: userState.firstName,
        lastName: userState.lastName,
        avatarUrl: userState.avatarUrl,
        ...obj,
      };
      if (!obj.avatarUrl) delete updateBody.avatarUrl;

      const submitStudentId = obj.studentId?.trim();
      if (submitStudentId !== '') {
        if (userState.studentId && submitStudentId !== userState.studentId)
          updateBody.studentId = submitStudentId;
        else if (!userState.studentId && submitStudentId)
          updateBody.studentId = submitStudentId;
        else delete updateBody.studentId;
      } else delete updateBody.studentId;

      const res = await accountApi.updateInfo(userState.id, updateBody);
      if (res) {
        dispatch(showSuccessNoti('Update successfully'));
        dispatch(storeAuthInfo(res));
        return;
      }
      dispatch(showErrorNoti('Update fail'));
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  const changePassword = async (data: IChangePassword) => {
    try {
      const obj: any = Object.assign({}, data);
      delete obj.confirmNewPassword;
      const res = await accountApi.changePassword(userState.id, obj);
      if (res) {
        dispatch(showSuccessNoti('Update successfully'));
        dispatch(storeAuthInfo(res));
        return;
      }
      dispatch(showErrorNoti('Update fail'));
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  return (
    <MainTemplate isLoading={userState.isFetching}>
      <Head>
        <title>My Profile | Classroom</title>
      </Head>

      <div className={styles.container}>
        <div className={styles.section}>
          <ProfileTopSection
            mode={sectionsViewMode.top ? 'view' : 'edit'}
            userInfo={topInfo}
            onChangeMode={switchTopSectionMode}
            onSave={updateProfile}
          />
        </div>

        <div className={styles.section}>
          <PasswordSection
            mode={sectionsViewMode.middle ? 'view' : 'edit'}
            onChangeMode={switchMiddleSectionMode}
            onSave={changePassword}
          />
        </div>

        <div className={styles.section}>
          <AdditionProfileSection
            mode={sectionsViewMode.addition ? 'view' : 'edit'}
            userInfo={additionalInfo}
            onChangeMode={switchAdditionSectionMode}
            onSave={updateProfile}
          />
        </div>
      </div>
    </MainTemplate>
  );
};

export default ProfileSettingPage;
