import type { NextPage } from 'next';
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Img from 'next/image';
import { useRouter } from 'next/router';
import Box from '@mui/material/Box';
import { CircularProgress } from '@mui/material';
import ErrorIcon from '@mui/icons-material/Error';
import ClassImage from '/public/class.png';
import MainTemplate from '../../../components/template/main';
import { useAppDispatch } from '../../../redux/store';
import classApi from '../../../api/classApi';
import {
  showErrorNoti,
  showSuccessNoti,
} from '../../../redux/slices/notification';
import styles from './index.module.scss';

const JoinInvitationPage: NextPage = () => {
  const router = useRouter();

  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string>('');
  const [countdown, setCountdown] = useState<number>(-1);

  const joinClass = async (token: string) => {
    try {
      setLoading(true);
      const res: any = await classApi.joinByEmail({ token });
      if (res) {
        dispatch(showSuccessNoti(res.message));
        router.replace('/');
        return;
      }
      setLoading(false);
      router.replace('/');
      setError('Join class fail');
      dispatch(showErrorNoti('Join class fail'));
    } catch (error: any) {
      const err = JSON.parse(error.message).message;
      console.log(err || error.message);

      setLoading(false);
      setCountdown(3);
      setError(err || 'Join class failed');
      dispatch(showErrorNoti(JSON.parse(error.message).message));
    }
  };

  useEffect(() => {
    const search = window.location.search;
    const params = new URLSearchParams(search);
    const token = params.get('token');
    if (token) joinClass(token);
    else {
      setTimeout(() => {
        router.replace('/');
      }, 1000);
    }
  }, []);

  useEffect(() => {
    if (countdown < 0) return;

    const intervalId = setInterval(() => {
      setCountdown(countdown - 1);
    }, 1000);

    if (countdown === 0) router.replace('/');

    return () => clearInterval(intervalId);
  }, [countdown]);

  return (
    <MainTemplate isLoading={false}>
      <Head>
        <title>Join Class | Classroom</title>
      </Head>

      <div className={styles.container}>
        <Img src={ClassImage} alt="classroom" width={200} height={200} />
        {loading ? (
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              p: 1,
              m: 1,
            }}
          >
            <CircularProgress size={20} style={{ marginRight: '5px' }} />
            <p>Please wait while we let you in</p>
          </Box>
        ) : (
          <></>
        )}

        {error ? (
          <>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'space-between',
                p: 1,
                m: 1,
              }}
            >
              <ErrorIcon color="error" style={{ marginRight: '5px' }} />
              <p className="error">{error}</p>
            </Box>
            <p>You will be redirected to home page in {countdown}</p>
          </>
        ) : (
          <></>
        )}
      </div>
    </MainTemplate>
  );
};

export default JoinInvitationPage;
