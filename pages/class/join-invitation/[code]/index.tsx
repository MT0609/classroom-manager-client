import { NextPage } from 'next';
import React from 'react';
import Head from 'next/head';
import Img from 'next/image';
import { useRouter } from 'next/router';
import { Button } from '@mui/material';
import Box from '@mui/material/Box';
import ClassImage from '/public/class.png';
import classApi from '../../../../api/classApi';
import MainTemplate from '../../../../components/template/main';
import { useAppDispatch } from '../../../../redux/store';
import {
  showErrorNoti,
  showSuccessNoti,
} from '../../../../redux/slices/notification';
import styles from './index.module.scss';

const ClassJoinByLink: NextPage = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { code } = router.query;

  const joinClass = async () => {
    if (!code) return;
    try {
      const res = await classApi.joinByCode({ code: `${code}` });
      if (res) {
        router.replace(`/class/${res.data.id}/home`);
        dispatch(showSuccessNoti('Joined class!'));
        return;
      }
      dispatch(showErrorNoti('Error joining class'));
    } catch (error: any) {
      const err = JSON.parse(error.message).message;
      console.log(err);
      dispatch(showErrorNoti(err));
    }
  };

  return (
    <MainTemplate isLoading={false}>
      <Head>
        <title>Class Invitation | Classroom</title>
      </Head>

      <div className={styles.container}>
        <Img src={ClassImage} alt="classroom" width={200} height={200} />
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            p: 1,
            m: 1,
          }}
        >
          <p>You can now join this class by clicking button right below</p>
        </Box>
        <Button variant="contained" onClick={joinClass}>
          Join Class
        </Button>
      </div>
    </MainTemplate>
  );
};

export default ClassJoinByLink;
