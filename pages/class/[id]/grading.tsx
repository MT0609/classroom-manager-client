import type { NextPage } from 'next';
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useAppDispatch, useAppSelector } from '../../../redux/store';
import { getClass, getClassGradeBoard } from '../../../redux/slices/class';
import MainTemplate from '../../../components/template/main';
import GradingOption from '../../../components/class/grading/gradingOption';
import { downloadFile, uploadFile } from '../../../config/firebase';
import {
  showErrorNoti,
  showInfoNoti,
  showSuccessNoti,
  closeNotification,
} from '../../../redux/slices/notification';
import classApi from '../../../api/classApi';
import GradeBoard from '../../../components/class/grading/gradeBoard';
import { checkIsCreator } from '../../../utils';
import styles from '../../../styles/Grading.module.scss';

const ClassGradingPage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;

  const dispatch = useAppDispatch();
  const classState = useAppSelector((state) => state.class);
  const classroom = classState.class;
  const gradeBoard = classroom.gradeBoard || {};

  const [isClassCreator, setIsClassCreator] = useState<boolean>(false);

  useEffect(() => {
    function getClassRoomInfo(classId: string) {
      try {
        dispatch(getClass(classId));
      } catch (error: any) {
        console.log(error.message);
      }
    }

    if (id) getClassRoomInfo(`${id}`);
    // if (classroom.id && !checkIsCreator(classroom.createdUser?.id))
    //   router.replace(`/class/${classroom.id}/home`);
    if (classroom.id)
      setIsClassCreator(checkIsCreator(classroom.createdUser?.id));
  }, [id, dispatch, classroom.id]);

  useEffect(() => {
    if (classroom.id) getGradeBoard(classroom.id);
  }, [classroom.id]);

  const onDownloadStudentListTemplate = () => {
    downloadFile('Student list import.xlsx', 'excel-templates');
  };

  const getGradeBoard = async (classId: string | number) => {
    try {
      if (classId) dispatch(getClassGradeBoard(classId));
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  const onUploadStudentList = async (file: any) => {
    try {
      dispatch(showInfoNoti('Uploading file...'));

      const fileLink = await uploadFile(
        file,
        `web/class-${classroom.name}-${classroom.id}`
      );
      const res: any = await classApi.importStudentForGrade(
        classroom.id,
        `${fileLink}`
      );
      if (res) {
        getGradeBoard(classroom.id);
        dispatch(showSuccessNoti('Upload file successfully'));
        return;
      }
      dispatch(showErrorNoti('Some error while uploading file...'));
    } catch (error: any) {
      dispatch(showErrorNoti('Some error while uploading file...'));
      console.log(error.message);
    }
  };

  const onUpdateMarkOnBoard = async (data: any) => {
    try {
      const res = await classApi.updateGradeBoardCell(classroom.id, data);
      if (res) {
        getGradeBoard(classroom.id);
        dispatch(showSuccessNoti('Update mark successfully'));
      }
    } catch (error: any) {
      dispatch(
        showErrorNoti(
          JSON.parse(error.message).message || 'Error updating mark'
        )
      );
      console.log(error.message);
    }
  };

  const onExportGradingForPointStructure = async (pointStructure: any) => {
    try {
      dispatch(showInfoNoti('Exporting file...'));
      const { name = '', code = '' } = pointStructure;

      const res: any = await classApi.exportGrade(classroom.id, code);
      if (res) {
        const fileUrl = res.url;
        const xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';
        xhr.onload = (event) => {
          let blob = new Blob([xhr.response]);
          let href = URL.createObjectURL(blob);
          let a = document.createElement('a') as HTMLAnchorElement;
          a.href = href;
          a.setAttribute(
            'download',
            `Grading-${classroom.name}-${name}-${code}.xlsx`
          );
          a.click();
          URL.revokeObjectURL(href);
        };
        xhr.open('GET', fileUrl);
        xhr.responseType = 'blob';
        xhr.send();
      }
      dispatch(closeNotification());
    } catch (error: any) {
      dispatch(
        showErrorNoti(
          JSON.parse(error.message).message || 'Error updating mark'
        )
      );
      console.log(error.message);
    }
  };

  const onUploadGrading = async (file: any) => {
    try {
      dispatch(showInfoNoti('Uploading and importing grades...'));

      const fileLink = await uploadFile(
        file,
        `web/class-${classroom.name}-${classroom.id}`
      );
      const res: any = await classApi.importGrade(classroom.id, `${fileLink}`);
      if (res) {
        getGradeBoard(classroom.id);
        dispatch(showSuccessNoti('Import gradings successfully'));
        return;
      }
      dispatch(showErrorNoti('Some error while uploading file...'));
    } catch (error: any) {
      dispatch(showErrorNoti('Some error while uploading file...'));
      console.log(error.message);
    }
  };

  const onToggleFinalForPointStructure = async (pointStructure: any) => {
    try {
      const newPointStructure = [...classroom.pointStructure];
      const index = newPointStructure.findIndex(
        (item: any) => item.code === pointStructure.code
      );
      if (index < 0) return;

      const tempPointStructure = JSON.parse(JSON.stringify(pointStructure));
      tempPointStructure.isFinalized = !tempPointStructure.isFinalized;
      newPointStructure[index] = tempPointStructure;

      dispatch(
        showInfoNoti(
          `${
            tempPointStructure.isFinalized ? 'Finalizing' : 'Unfinalizing'
          } point structure for ${pointStructure.name}...`
        )
      );

      const body = {
        pointStructure: newPointStructure,
      };
      const res: any = await classApi.updatePointStructure(`${id}`, body);
      if (res) {
        dispatch(getClassGradeBoard(`${id}`));
        dispatch(showSuccessNoti(res.message));
      }
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  const onDownloadGradingBoard = async () => {
    try {
      dispatch(showInfoNoti('Exporting file...'));

      const res: any = await classApi.exportGradingBoard(classroom.id);
      if (res) {
        const fileUrl = res.url;
        const xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';
        xhr.onload = (event) => {
          let blob = new Blob([xhr.response]);
          let href = URL.createObjectURL(blob);
          let a = document.createElement('a') as HTMLAnchorElement;
          a.href = href;
          a.setAttribute('download', `Grading-${classroom.name}.xlsx`);
          a.click();
          URL.revokeObjectURL(href);
        };
        xhr.open('GET', fileUrl);
        xhr.responseType = 'blob';
        xhr.send();
      }
      dispatch(closeNotification());
    } catch (error: any) {
      dispatch(
        showErrorNoti(
          JSON.parse(error.message).message || 'Error updating mark'
        )
      );
      console.log(error.message);
    }
  };

  const onRequestReviewForGrading = async (data: any) => {
    try {
      const res: any = await classApi.updateGradeBoardCell(`${id}`, data);
      if (res) {
        getGradeBoard(classroom.id);
        dispatch(showSuccessNoti('Sent grading review request'));
      }
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  const onCommentGradingReview = async (data: any) => {
    try {
      const res: any = await classApi.updateGradeBoardCell(`${id}`, data);
      if (res) getGradeBoard(classroom.id);
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  const onApproveReviewForGrading = async (data: any) => {
    try {
      const res: any = await classApi.updateGradeBoardCell(`${id}`, data);
      if (res) {
        getGradeBoard(classroom.id);
        dispatch(showSuccessNoti('Approved grade review request'));
      }
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  return (
    <MainTemplate isLoading={false} padding>
      <Head>
        <title>{classroom.name} | Grading | Classroom</title>
      </Head>

      {classroom.id !== '' ? (
        <main className={styles['grading-page']}>
          <div className={styles.top}>
            <span>Class: {classroom.name} - Grading Board</span>
            {isClassCreator ? (
              <GradingOption
                onDownloadStudentListTemplate={onDownloadStudentListTemplate}
                onUploadStudentList={onUploadStudentList}
                onUploadGrading={onUploadGrading}
                onDownloadGradingBoard={onDownloadGradingBoard}
              />
            ) : (
              <></>
            )}
          </div>
          <GradeBoard
            gradeBoard={gradeBoard}
            isClassCreator={isClassCreator}
            onUpdateMarkOnBoard={onUpdateMarkOnBoard}
            onExportGradingForPointStructure={onExportGradingForPointStructure}
            onToggleFinalForPointStructure={onToggleFinalForPointStructure}
            onRequestReviewForGrading={onRequestReviewForGrading}
            onCommentGradingReview={onCommentGradingReview}
            onApproveReviewForGrading={onApproveReviewForGrading}
          />
        </main>
      ) : (
        <p>Class not found</p>
      )}
    </MainTemplate>
  );
};

export default ClassGradingPage;
