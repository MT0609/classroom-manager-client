import type { NextPage } from 'next';
import React, { useEffect } from 'react';
import Head from 'next/head';
import Img from 'next/image';
import classroomBackground from '/public/home-classroom-background.jpg';
import IconButton from '@mui/material/IconButton';
import CopyIcon from '@mui/icons-material/ContentCopy';
import { useRouter } from 'next/router';
import ClassroomTemplate from '../../../components/template/class';
import { useAppDispatch, useAppSelector } from '../../../redux/store';
import { getClass } from '../../../redux/slices/class';
import styles from './index.module.scss';

const ClassAboutPage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;

  const dispatch = useAppDispatch();
  const classState = useAppSelector((state) => state.class);
  const classroom = classState.class;

  useEffect(() => {
    function getClassRoomInfo(classId: string) {
      try {
        dispatch(getClass(classId));
      } catch (error: any) {
        console.log(error.message);
      }
    }

    if (id) getClassRoomInfo(`${id}`);
  }, [id, dispatch]);

  const copyInvitationLink = async () => {
    if (typeof window === 'undefined') return;

    const invitationLink = `${window.location.protocol}//${window.location.host}/class/join-invitation/${classroom.code}`;

    if ('clipboard' in navigator) {
      return await navigator.clipboard.writeText(invitationLink);
    } else {
      return document.execCommand('copy', true, invitationLink);
    }
  };

  return (
    <ClassroomTemplate classInfo={classroom}>
      <Head>
        <title>{classroom.name} | Classroom</title>
      </Head>

      {classroom.id !== '' ? (
        <div className={`${styles.main__container} ${styles.box}`}>
          <div className={styles.main__section}>
            {classroom.subject && <p>Subject: {classroom.subject}</p>}
            {classroom.description && <p>Section: {classroom.description}</p>}
            {classroom.room && <p>Room: {classroom.room}</p>}
            <p>Code: {classroom.code}</p>
          </div>

          <div className={styles.page__section}>
            <div>Invite more people to join this class</div>
            <div>
              <IconButton onClick={copyInvitationLink}>
                <CopyIcon />
              </IconButton>
              <span>Copy invitation link</span>
            </div>
          </div>
        </div>
      ) : (
        <p>Class not found</p>
      )}
    </ClassroomTemplate>
  );
};

export default ClassAboutPage;
