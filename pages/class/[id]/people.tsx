import type { NextPage } from 'next';
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import ClassroomTemplate from '../../../components/template/class';
import { useAppDispatch, useAppSelector } from '../../../redux/store';
import { getClass } from '../../../redux/slices/class';
import MemberList from '../../../components/class/memberList';
import { IUserInfo, UserRole } from '../../../models';
import { ISendInvitationMail } from '../../../models/form';
import SendMailModal from '../../../components/class/modal/sendMail';
import classApi from '../../../api/classApi';
import {
  showErrorNoti,
  showSuccessNoti,
} from '../../../redux/slices/notification';
import { checkIsCreator } from '../../../utils';
import styles from './index.module.scss';

const ClassMembers: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;

  const dispatch = useAppDispatch();
  const classState = useAppSelector((state) => state.class);
  const classroom = classState.class;

  const [teachers, setTeachers] = useState<IUserInfo[]>([]);
  const [students, setStudents] = useState<IUserInfo[]>([]);
  const [sendMailOpen, setSendMailOpen] = useState<boolean>(false);
  const [roleSelected, setRoleSelected] = useState<UserRole>('user');
  const [isCreator, setIsCreator] = useState<boolean>();

  useEffect(() => {
    if (id) getClassRoomInfo(`${id}`);
    setIsCreator(checkIsCreator(`${classroom.createdUser?.id}`));
  }, [id, classroom.id]);

  useEffect(() => {
    if (classroom.members.length === 0) return;
    setTeachers(
      classroom.members.filter((member) => member.role === 'lecture')
    );
    setStudents(
      classroom.members.filter((member) => member.role !== 'lecture')
    );
  }, [classroom.members]);

  const getClassRoomInfo = async (classId: string) => {
    try {
      dispatch(getClass(classId));
    } catch (error: any) {
      console.log(error.message);
    }
  };

  const openSendMailDialogWithRole = (role: UserRole) => {
    setRoleSelected(role);
    setSendMailOpen(true);
  };

  const sendMailInvitation = async (data: ISendInvitationMail) => {
    try {
      const res: any = await classApi.inviteByEmail(data);
      if (res) {
        dispatch(showSuccessNoti(res.message));
        return;
      }
      dispatch(showErrorNoti('Error sending mail'));
    } catch (error: any) {
      const err = JSON.parse(error.message).message;
      console.log(err);
      dispatch(showErrorNoti(err));
    }
  };

  return (
    <ClassroomTemplate classInfo={classroom}>
      <Head>
        <title>{classroom.name} | People | Classroom</title>
      </Head>

      {classroom.name ? (
        <>
          <div className={styles.main__container}>
            <div className={styles.main__section}>
              <MemberList
                title="Teachers"
                role="lecture"
                people={teachers}
                isClassCreator={isCreator || false}
                onOpenSendMailDialog={(role: UserRole) =>
                  openSendMailDialogWithRole(role)
                }
              />
            </div>

            <div className={styles.main__section}>
              <MemberList
                title="Students"
                role="user"
                people={students}
                isClassCreator={isCreator || false}
                onOpenSendMailDialog={(role: UserRole) =>
                  openSendMailDialogWithRole(role)
                }
              />
            </div>
          </div>

          <SendMailModal
            open={sendMailOpen}
            role={roleSelected}
            classCode={classroom.code}
            handleClose={() => setSendMailOpen(false)}
            onSendMail={sendMailInvitation}
          />
        </>
      ) : (
        <p>Class not found</p>
      )}
    </ClassroomTemplate>
  );
};

export default ClassMembers;
