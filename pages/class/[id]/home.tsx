import type { NextPage } from 'next';
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Grid } from '@mui/material';
import ClassroomTemplate from '../../../components/template/class';
import { useAppDispatch, useAppSelector } from '../../../redux/store';
import { getClass } from '../../../redux/slices/class';
import { checkIsCreator } from '../../../utils';
import styles from './index.module.scss';

const ClassHomePage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;

  const dispatch = useAppDispatch();
  const classState = useAppSelector((state) => state.class);
  const classroom = classState.class;
  const [isCreator, setIsCreator] = useState<boolean>();

  useEffect(() => {
    if (id) getClassRoomInfo(`${id}`);
    setIsCreator(checkIsCreator(`${classroom.createdUser?.id}`));
  }, [id, classroom.id]);

  const getClassRoomInfo = async (classId: string) => {
    try {
      dispatch(getClass(classId));
    } catch (error: any) {
      console.log(error.message);
    }
  };

  return (
    <ClassroomTemplate classInfo={classroom}>
      <Head>
        <title>{classroom.name} | Classroom</title>
      </Head>

      {classroom.id !== '' ? (
        <div>
          <Grid container spacing={2}>
            <Grid item md={4} xs={12}>
              <div className={styles.box}>
                {classroom.pointStructure.length > 0 ? (
                  <ul style={{ paddingLeft: 'inherit', margin: 0 }}>
                    {classroom.pointStructure.map((item, index: number) => (
                      <li key={`point-structure-${index}`}>
                        {item.name}: {item.point}
                      </li>
                    ))}
                  </ul>
                ) : (
                  <p>No point structures</p>
                )}
                {isCreator ? (
                  <Link href={`/class/${classroom.id}/point-structure`}>
                    <a>Manage point structure</a>
                  </Link>
                ) : (
                  <></>
                )}
              </div>
            </Grid>
            <Grid item md={8} xs={12}>
              <div className={styles.box}>
                <p>Class Name: {classroom.name}</p>
                <p>Code: {classroom.code}</p>
                {classroom.subject && <p>Subject: {classroom.subject}</p>}
                {classroom.description && (
                  <p>Section: {classroom.description}</p>
                )}
                {classroom.room && <p>Room: {classroom.room}</p>}{' '}
              </div>
            </Grid>
          </Grid>
        </div>
      ) : (
        <p>Class not found</p>
      )}
    </ClassroomTemplate>
  );
};

export default ClassHomePage;
