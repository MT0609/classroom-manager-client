import type { NextPage } from 'next';
import React, { useEffect, useState, useRef } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { Box, Button } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import ClassroomTemplate from '../../../components/template/class';
import PointStructureList from '../../../components/class/pointStructure';
import { useAppDispatch, useAppSelector } from '../../../redux/store';
import { getClass } from '../../../redux/slices/class';
import PointStructureModal from '../../../components/class/modal/pointStructureUpdate';
import {
  showErrorNoti,
  showSuccessNoti,
} from '../../../redux/slices/notification';
import { IPointStructure } from '../../../models';
import classApi from '../../../api/classApi';
import { checkIsCreator } from '../../../utils';
import styles from './index.module.scss';

const ClassPointStructurePage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;

  const dispatch = useAppDispatch();
  const classState = useAppSelector((state) => state.class);
  const classroom = classState.class;
  const pointStructure = classroom.pointStructure;

  const [tempPointStructure, setTempPointStructure] = useState(pointStructure);
  const [pointStructureModalOpen, setPointStructureModalOpen] =
    useState<boolean>(false);
  const [pointStructureModalType, setPointStructureModalType] =
    useState<string>('create');
  const [pointStructureInfo, setPointStructureInfo] = useState<IPointStructure>(
    {
      name: '',
      code: '',
      point: 0,
      isFinalized: false,
    }
  );
  const timeRef = useRef<any>();

  useEffect(() => {
    function getClassRoomInfo(classId: string) {
      try {
        dispatch(getClass(classId));
      } catch (error: any) {
        console.log(error.message);
      }
    }

    if (id) getClassRoomInfo(`${id}`);
    if (classroom.id && !checkIsCreator(classroom.createdUser?.id))
      router.replace(`/class/${classroom.id}/home`);
  }, [id, classroom.id, dispatch]);

  useEffect(() => {
    setTempPointStructure(pointStructure);
  }, [pointStructure]);

  const onDragEnd = async (dragIndex: number, hoverIndex: number) => {
    try {
      const draggedItem = tempPointStructure[dragIndex];

      const result = [...tempPointStructure];
      result.splice(dragIndex, 1);
      result.splice(hoverIndex, 0, draggedItem);

      setTempPointStructure(result);

      const body = {
        pointStructure: result,
      };

      if (timeRef.current) clearInterval(timeRef.current);

      timeRef.current = setTimeout(async () => {
        const res: any = await classApi.updatePointStructure(`${id}`, body);
        if (res) {
          dispatch(getClass(`${id}`));
        }
      }, 1500);
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  const openPointStructureDialogWithType = (type: string) => {
    setPointStructureModalType(type);
    setPointStructureModalOpen(true);
  };

  const createNewPointStructure = async (data: IPointStructure) => {
    const body = {
      pointStructure: [...pointStructure, data],
    };
    try {
      const res: any = await classApi.updatePointStructure(`${id}`, body);
      if (res) {
        dispatch(showSuccessNoti(res.message));
        dispatch(getClass(`${id}`));
      }
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  const onOpenEditDialog = (index: string | number) => {
    setPointStructureInfo(pointStructure[+index]);
    openPointStructureDialogWithType('edit');
  };

  const editPointStructure = async (data: any) => {
    try {
      const newPointStructure = [...pointStructure];
      const index = newPointStructure.findIndex(
        (item: any) => item.code === data.code
      );
      if (index < 0) return;
      newPointStructure[index] = data;

      const body = {
        pointStructure: newPointStructure,
      };
      const res: any = await classApi.updatePointStructure(`${id}`, body);
      if (res) {
        dispatch(showSuccessNoti(res.message));
        dispatch(getClass(`${id}`));
      }
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  const onDeletePointStructure = async (index: string | number) => {
    try {
      const newPointStructure = [...pointStructure];
      newPointStructure.splice(+index, 1);

      const body = {
        pointStructure: newPointStructure,
      };
      const res: any = await classApi.updatePointStructure(`${id}`, body);
      if (res) {
        dispatch(showSuccessNoti(res.message));
        dispatch(getClass(`${id}`));
      }
    } catch (error: any) {
      dispatch(showErrorNoti(JSON.parse(error.message).message));
      console.log(error.message);
    }
  };

  return (
    <ClassroomTemplate classInfo={classroom}>
      <Head>
        <title>{classroom.name} | Classroom</title>
      </Head>

      {classroom.id !== '' ? (
        <div className={styles.main__container}>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
            style={{
              marginBottom: '1rem',
            }}
          >
            <span>Class Point Structure</span>
            <Button
              variant="outlined"
              onClick={() => openPointStructureDialogWithType('create')}
            >
              <AddIcon />
              Add new point structure
            </Button>
          </Box>
          <DndProvider backend={HTML5Backend}>
            <PointStructureList
              pointStructures={tempPointStructure}
              onDragEnd={onDragEnd}
              onOpenEditDialog={onOpenEditDialog}
              onDeletePointStructure={onDeletePointStructure}
            />
          </DndProvider>

          <PointStructureModal
            open={pointStructureModalOpen}
            type={pointStructureModalType}
            pointStructureInfo={pointStructureInfo}
            handleClose={() => {
              setPointStructureInfo({
                name: '',
                code: '',
                point: 0,
              });
              setPointStructureModalOpen(false);
            }}
            clickCreate={createNewPointStructure}
            clickEdit={editPointStructure}
          />
        </div>
      ) : (
        <p>Class not found</p>
      )}
    </ClassroomTemplate>
  );
};

export default ClassPointStructurePage;
