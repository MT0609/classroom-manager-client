import type { NextPage } from 'next';
import { useEffect } from 'react';
import { useRouter } from 'next/router';

const ClassMainPage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    if (id) router.replace(`/class/${id}/home`);
  }, [id]);

  return null;
};

export default ClassMainPage;
