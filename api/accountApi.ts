import axiosClient from './axiosClient';
import {
  IChangePasswordSubmit,
  IFormLogin,
  IFormSocialLogin,
  IRegisterSubmit,
} from '../models/form';
import { ITokenBody, IUserInfo } from '../models';

const accountApi = {
  signIn: (body: IFormLogin) => {
    const url = '/auth/login';
    return axiosClient.post(url, body);
  },
  socialSignIn: (body: IFormSocialLogin) => {
    const url = '/auth/external-login';
    return axiosClient.post(url, body);
  },
  signUp: (body: IRegisterSubmit) => {
    const url = '/auth/register';
    return axiosClient.post(url, body);
  },
  resetPassword: (body: { newPassword: string; token: string }) => {
    const url = '/auth/verify-reset-link';
    return axiosClient.post(url, body);
  },
  activateAccount: (data: ITokenBody) => {
    const url = '/auth/verify-activate-link';
    return axiosClient.post(url, data);
  },
  resendActivateEmail: (data: { email: string }) => {
    const url = '/auth/resend-activate-email';
    return axiosClient.post(url, data);
  },
  sendResetPasswordMail: (data: { email: string }) => {
    const url = '/auth/resend-reset-email';
    return axiosClient.post(url, data);
  },
  getMyInfo: () => {
    const url = `/users/me`;
    return axiosClient.get<IUserInfo>(url);
  },
  updateInfo: (userId: string | number, body: any) => {
    const url = `/users/${userId}`;
    return axiosClient.patch(url, body);
  },
  changePassword: (userId: string | number, body: IChangePasswordSubmit) => {
    const url = `/users/${userId}/password`;
    return axiosClient.patch(url, body);
  },
  getNotifications: () => {
    const url = `/users/me/notifications`;
    return axiosClient.get(url);
  },
};

export default accountApi;
