import axiosClient from './axiosClient';
import { IClassroom, ITokenBody, IPaginatedResult } from '../models';
import { IClassCreate, IClassJoin, ISendInvitationMail } from '../models/form';

const classApi = {
  getAll: () => {
    const url = '/classes';
    return axiosClient.get<IPaginatedResult<IClassroom>>(url);
  },
  getOne: (classId: string) => {
    const url = `/classes/${classId}`;
    return axiosClient.get<IClassroom>(url);
  },
  createOne: (data: IClassCreate) => {
    const url = `/classes`;
    return axiosClient.post(url, data);
  },
  joinByCode: (data: IClassJoin) => {
    const url = `/classes/join-by-code`;
    return axiosClient.post(url, data);
  },
  inviteByEmail: (data: ISendInvitationMail) => {
    const url = `/classes/invite-by-email`;
    return axiosClient.post(url, data);
  },
  joinByEmail: (data: ITokenBody) => {
    const url = `/classes/join-by-email`;
    return axiosClient.post(url, data);
  },
  updatePointStructure: (classId: string, data: any) => {
    const url = `/classes/${classId}/point-structures`;
    return axiosClient.put(url, data);
  },
  importStudentForGrade: (classId: string | number, fileUrl: string) => {
    const url = `/classes/${classId}/student-import`;
    return axiosClient.post(url, { url: fileUrl });
  },
  getGradeBoard: (classId: string | number) => {
    const url = `/classes/${classId}/grade-board`;
    return axiosClient.get(url);
  },
  updateGradeBoardCell: (classId: string | number, data: any) => {
    const url = `/classes/${classId}/grade-board/update`;
    return axiosClient.post(url, data);
  },
  exportGrade: (classId: string | number, code: string) => {
    const url = `/classes/${classId}/student-grade-export`;
    return axiosClient.post(url, { code });
  },
  exportGradingBoard: (classId: string | number) => {
    const url = `/classes/${classId}/student-grade-board-export`;
    return axiosClient.post(url);
  },
  importGrade: (classId: string | number, fileUrl: string) => {
    const url = `/classes/${classId}/student-grade-import`;
    return axiosClient.post(url, { url: fileUrl });
  },
};

export default classApi;
