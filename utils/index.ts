import Cookies from 'js-cookie';
import jwt from 'jsonwebtoken';

export function validateEmail(email: string): boolean {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export function checkIsCreator(userId: string | number | undefined): boolean {
  try {
    if (!userId) return false;

    const accessToken = Cookies.get(process.env.NEXT_PUBLIC_ACCESS_TOKEN || '');
    if (!accessToken) return false;

    const decoded = jwt.verify(
      accessToken,
      process.env.NEXT_PUBLIC_JWT_KEY || ''
    );
    if (!decoded) return false;

    if (decoded.sub === userId) return true;

    return false;
  } catch (error: any) {
    console.log(error.message);
    return false;
  }
}
