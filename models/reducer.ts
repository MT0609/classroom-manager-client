import { IClassroom, IPaginatedResult, IUserInfo } from '.';

export interface IUserReducer extends IUserInfo {
  isAuthenticated: boolean;
  isFetching: boolean;
}

type NotiState = 'success' | 'error' | 'warning' | 'info';
export interface INotiReducer {
  state: NotiState;
  message: string;
  open: boolean;
}

export interface IClassroomsReducer {
  classrooms: IPaginatedResult<IClassroom>;
  class: IClassroom;
  isLoading: boolean;
}

export interface INotificationList {
  notifications: IPaginatedResult<any>;
  isLoading: boolean;
}
