import { UserRole } from '.';

export interface IFormLogin {
  email: string;
  password: string;
}

export interface IFormSocialLogin {
  externalToken: string;
}

export interface IFormRegister {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
}

export interface IRegisterSubmit {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
}

export interface IResetPassword {
  newPassword: string;
  confirmNewPassword: string;
}

export interface IChangePassword {
  oldPassword: string;
  newPassword: string;
  confirmNewPassword: string;
}

export interface IChangePasswordSubmit {
  oldPassword: string;
  newPassword: string;
}

export interface IClassJoin {
  code: string;
}

export interface IClassCreate {
  name: string;
  description: string;
  subject: string;
  room: string;
}

export interface ISendInvitationMail {
  email: string;
  code: string;
  role: UserRole;
}

export interface IPointStructureForm {
  name: string;
  point: number;
  isFinalized: boolean;
}
