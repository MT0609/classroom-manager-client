export interface IUserInfo {
  id: number | string;
  firstName: string;
  lastName: string;
  email: string;
  avatarUrl: string;
  studentId?: string | number | null;
  role?: string;
  theme?: AppTheme;
  description?: string;
  createdAt?: string;
  updatedAt?: string;
}

export interface IClassroom {
  id: string | number;
  name: string;
  code: string;
  backgroundUrl?: string;
  description?: string;
  subject: string;
  room: string;
  createdUser?: IUserInfo;
  createdAt?: string;
  updatedAt?: string;
  members: IUserInfo[];
  pointStructure: IPointStructure[];
  gradeBoard?: any;
}

interface IPagination {
  total: number;
  size: number;
  totalPages: number;
  page: number;
}

export interface IPaginatedResult<T> {
  data: T[];
  pagination: IPagination;
}

export interface ITokenBody {
  token: string;
}

export interface IPointStructure {
  name: string;
  point: number;
  code?: string;
  isFinalized?: boolean;
}

export type AppTheme = 'light' | 'dark';

export type UserRole = 'user' | 'lecture';

export enum ReferenceType {
  FEED = 'feed',
  EXERCISE = 'exercise',
  FINALIZE_COLUMN = 'finalize_column',
  FINALIZE_COLUMN_REVIEW = 'finalize_column_review',
  REQUEST_GRADE_VIEW = 'request_grade_view',
  REPLY_GRADE_VIEW = 'reply_grade_view',
}
