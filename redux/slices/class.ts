import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { IClassroomsReducer } from '../../models/reducer';
import classApi from '../../api/classApi';

export const getAllClass: any = createAsyncThunk(
  'classrooms/getAll',
  async () => {
    try {
      const result: any = await classApi.getAll();
      if (result) return result;

      return null;
    } catch (error) {
      throw error;
    }
  }
);

export const getClass: any = createAsyncThunk(
  'classrooms/getClass',
  async (classId: string) => {
    try {
      const result: any = await classApi.getOne(classId);
      if (result) return result;

      return null;
    } catch (error) {
      throw error;
    }
  }
);

export const getClassGradeBoard: any = createAsyncThunk(
  'classrooms/getClassGradeBoard',
  async (classId: string | number) => {
    try {
      const result: any = await classApi.getGradeBoard(classId);
      if (result) return result;

      return null;
    } catch (error) {
      throw error;
    }
  }
);

const initialState: IClassroomsReducer = {
  classrooms: {
    data: [],
    pagination: {
      total: 4,
      size: 4,
      totalPages: 1,
      page: 1,
    },
  },
  class: {
    id: '',
    code: '',
    name: '',
    subject: '',
    room: '',
    members: [],
    createdUser: undefined,
    createdAt: '',
    updatedAt: '',
    pointStructure: [],
    gradeBoard: {},
  },
  isLoading: true,
};

const classroomSlice = createSlice({
  name: 'classrooms',
  initialState,
  reducers: {},
  extraReducers: {
    [getAllClass.pending]: (state) => {
      state.isLoading = true;
    },
    [getAllClass.fulfilled]: (state, action) => {
      if (!action.payload) {
        state.isLoading = false;
        return;
      }

      state.isLoading = false;
      state.classrooms = action.payload;
    },
    [getClass.pending]: (state) => {
      state.isLoading = true;
    },
    [getClass.fulfilled]: (state, action) => {
      if (!action.payload) {
        state.isLoading = false;
        state.class = Object.assign({}, initialState.class);
        return;
      }
      state.isLoading = false;
      state.class = action.payload;
    },
    [getClassGradeBoard.pending]: (state) => {
      state.isLoading = true;
    },
    [getClassGradeBoard.fulfilled]: (state, action) => {
      if (!action.payload) {
        state.isLoading = false;
        return;
      }
      state.isLoading = false;
      state.class.gradeBoard = action.payload;
    },
  },
});

const { reducer } = classroomSlice;
export default reducer;
