import { createSlice } from '@reduxjs/toolkit';
import { INotiReducer } from '../../models/reducer';

const initialState: INotiReducer = {
  open: false,
  message: '',
  state: 'success',
};

const notiSlice = createSlice({
  name: 'notification',
  initialState,
  reducers: {
    showSuccessNoti(state, action) {
      state.state = 'success';
      state.message = action.payload;
      state.open = true;
    },
    showErrorNoti(state, action) {
      state.state = 'error';
      state.message = action.payload;
      state.open = true;
    },
    showInfoNoti(state, action) {
      state.state = 'info';
      state.message = action.payload;
      state.open = true;
    },
    closeNotification(state) {
      state.open = false;
      state.message = '';
    },
  },
});

const { reducer, actions } = notiSlice;
export const {
  showSuccessNoti,
  showErrorNoti,
  showInfoNoti,
  closeNotification,
} = actions;
export default reducer;
