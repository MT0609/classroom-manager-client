import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import Cookies from 'js-cookie';
import accountApi from '../../api/accountApi';
import { IUserReducer } from '../../models/reducer';
import { firebase } from '../../config/firebase';

export const getMyInfo: any = createAsyncThunk(
  'userInfo/getMyInfo',
  async () => {
    try {
      const result: any = await accountApi.getMyInfo();
      if (result) return result;

      return {};
    } catch (error) {
      throw error;
    }
  }
);

const initialState: IUserReducer = {
  id: '',
  email: '',
  firstName: '',
  lastName: '',
  avatarUrl: '',
  studentId: null,
  theme: 'dark',
  isAuthenticated: false,
  isFetching: true,
};

const authSlice = createSlice({
  name: 'userInfo',
  initialState,
  reducers: {
    storeAuthInfo(state, action) {
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
      };
    },
    resetTheme(state) {
      state.theme = 'light';
    },
    signOut(state) {
      firebase.auth().signOut();
      Cookies.remove(process.env.NEXT_PUBLIC_ACCESS_TOKEN || '');
      Cookies.remove(process.env.NEXT_PUBLIC_REFRESH_TOKEN || '');
      return {
        ...state,
        ...initialState,
        isFetching: false,
      };
    },
  },
  extraReducers: {
    [getMyInfo.fulfilled]: (state, action) => {
      if (!action.payload?.email) {
        state.isFetching = false;
        return;
      }

      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
        isFetching: false,
      };
    },
    [getMyInfo.rejected]: (state, action) => {
      return {
        ...initialState,
        isFetching: false,
      };
    },
  },
});

const { reducer, actions } = authSlice;
export const { storeAuthInfo, signOut, resetTheme } = actions;
export default reducer;
