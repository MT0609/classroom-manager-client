import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { INotificationList } from '../../models/reducer';
import accountApi from '../../api/accountApi';

export const getUserNotifications: any = createAsyncThunk(
  'notificationList/getNotifications',
  async () => {
    try {
      const result: any = await accountApi.getNotifications();
      if (result) return result;

      return null;
    } catch (error) {
      throw error;
    }
  }
);

const initialState: INotificationList = {
  notifications: {
    data: [],
    pagination: {
      total: 4,
      size: 4,
      totalPages: 1,
      page: 1,
    },
  },
  isLoading: false,
};

const notiListSlice = createSlice({
  name: 'notificationList',
  initialState,
  reducers: {},
  extraReducers: {
    [getUserNotifications.pending]: (state, action) => {
      state.isLoading = true;
    },
    [getUserNotifications.fulfilled]: (state, action) => {
      state.notifications.data = [...action.payload.data].reverse();
      state.notifications.pagination = { ...action.payload.pagination };
      state.isLoading = false;
    },
    [getUserNotifications.rejected]: (state, action) => {
      return {
        ...state,
        isLoading: false,
      };
    },
  },
});

const { reducer, actions } = notiListSlice;
export default reducer;
