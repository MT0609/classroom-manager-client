import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { createWrapper } from 'next-redux-wrapper';
import AuthInfo from './slices/auth';
import Classroom from './slices/class';
import Notification from './slices/notification';
import NotificationList from './slices/notificationList';

export const store = configureStore({
  reducer: {
    authInfo: AuthInfo,
    class: Classroom,
    notification: Notification,
    notificationList: NotificationList,
  },
  devTools: process.env.NODE_ENV !== 'production',
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export const makeStore = () => store;
export const wrapper = createWrapper(makeStore);

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
